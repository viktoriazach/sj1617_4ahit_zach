﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyTask;

namespace TimeToolWPF
{
    /// <summary>
    /// Interaction logic for Actual.xaml
    /// </summary>
    public partial class Actual : Window
    {
        Tasks task;
        public Actual(Tasks task)
        {
            this.task = task;
            InitializeComponent();
            dt_plan.DisplayDateEnd = DateTime.Now;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int dur;
            if (!int.TryParse(tb_dur.Text, out dur))
            {
                DialogResult = false;
                return;
            }
            DialogResult = true;
            task.actDuration = dur;
            task.actEnd = dt_plan.SelectedDate.Value;
            Tasks.SaveInDB(task, true);
        }
    }
}
