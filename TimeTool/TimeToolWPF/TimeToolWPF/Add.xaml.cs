﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using MyTask;

namespace TimeToolWPF
{
    /// <summary>
    /// Interaction logic for Add.xaml
    /// </summary>
    public partial class Add : Window
    {
        List<Tasks> myt;
        public Add(List<Tasks> myt)
        {
            this.myt = myt;
            InitializeComponent();
            dt_plan.DisplayDateStart = DateTime.Now;
        }

        private void abort_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {
                if ((tb_name.Text == "") || (tb_desc.Text == ""))
                {
                    throw new Exception();
                }
                bool imp = true;
                bool urg = true;
                int dur;
                if(!int.TryParse(tb_dur.Text, out dur))
                {
                    throw new Exception();
                }
                DialogResult = true;
                
                if (cb_imp.SelectedIndex == 0)
                    imp = false;

                if (cb_urg.SelectedIndex ==0)
                    urg = false;

                myt.Add(new Tasks(Tasks.GetIndex(), this.tb_name.Text, this.tb_desc.Text, imp, urg, Convert.ToDateTime(this.dt_plan.Text), dur));
            }
            catch
            {
                MessageBox.Show("You have to insert some values!");
            }
            this.Close();
            this.Close();
        }
    }
}
