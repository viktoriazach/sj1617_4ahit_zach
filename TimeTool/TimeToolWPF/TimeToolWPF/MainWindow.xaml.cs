﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyTask;

namespace TimeToolWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Tasks> myTasks = new List<Tasks>();
        bool unselect = true;
        Tasks selected = null;
        public MainWindow()
        {
            InitializeComponent();
            myTasks = Tasks.GetData();
            Refresh(Tasks.SortAndReturn(myTasks));
            stateInvisible();
        }

        private void Refresh(List<Tasks>[] sorted)
        {
            listBox0.Items.Clear();
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();

            foreach (var item in sorted[0])
            {
                listBox0.Items.Add(item);
            }
            foreach (var item in sorted[1])
            {
                listBox1.Items.Add(item);
            }
            foreach (var item in sorted[2])
            {
                listBox2.Items.Add(item);
            }
            foreach (var item in sorted[3])
            {
                listBox3.Items.Add(item);
            }
            

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Info info = new Info();
            info.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Add add = new Add(myTasks);
            add.ShowDialog();
            myTasks = Tasks.GetData();
            Refresh(Tasks.SortAndReturn(myTasks));
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox1.SelectedItem == null)
                return;
            if (listBox1.SelectedIndex != -1)
                selected = (Tasks)listBox1.SelectedItem;
            UnSelectOthers(1);
              stateVisible();
        }

        private void UnSelectOthers(int i)
        {
            if (unselect)
            {
                switch (i)
                {
                    case 0:
                        unselect = false;
                        listBox1.SelectedIndex = -1;
                        listBox2.SelectedIndex = -1;
                        listBox3.SelectedIndex = -1;
                        unselect = true;
                        //stateInvisible();
                        break;
                    case 1:
                        unselect = false;
                        listBox0.SelectedIndex = -1;
                        listBox2.SelectedIndex = -1;
                        listBox3.SelectedIndex = -1;
                        unselect = true;
                       // stateInvisible();
                        break;
                    case 2:
                        unselect = false;
                        listBox0.SelectedIndex = -1;
                        listBox1.SelectedIndex = -1;
                        listBox3.SelectedIndex = -1;
                        unselect = true;
                        //stateInvisible();
                        break;
                    case 3:
                        unselect = false;
                        listBox0.SelectedIndex = -1;
                        listBox1.SelectedIndex = -1;
                        listBox2.SelectedIndex = -1;
                        //stateInvisible();
                        unselect = true;
                        break;
                }
            }
        }

        private void listBox0_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox0.SelectedItem == null)
                return;
            if (listBox0.SelectedIndex != -1)
                selected = (Tasks)listBox0.SelectedItem;
            UnSelectOthers(0);
        stateVisible();
        }

        private void listBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox2.SelectedItem == null)
                return;
            if (listBox2.SelectedIndex != -1)
                selected = (Tasks)listBox2.SelectedItem;
            UnSelectOthers(2);
              stateVisible();
        }

        private void listBox3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBox3.SelectedItem == null)
                return;
            if (listBox3.SelectedIndex != -1)
                selected = (Tasks)listBox3.SelectedItem;
            UnSelectOthers(3);
          stateVisible();
        }

        private void stateInvisible()
        {
            p1.Visibility = Visibility.Hidden;
            p2.Visibility = Visibility.Hidden;
            p3.Visibility = Visibility.Hidden;
            status.Visibility = Visibility.Hidden;
        }

        private void stateVisible()
        {
            p1.Visibility = Visibility.Hidden;
            p2.Visibility = Visibility.Hidden;
            p3.Visibility = Visibility.Hidden;
            status.Visibility = Visibility.Visible;
            if (selected.status == 0)
                p1.Visibility = Visibility.Visible;
            if (selected.status == 1)
                p2.Visibility = Visibility.Visible;
            if (selected.status == 2)
                p3.Visibility = Visibility.Visible;
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e) //1
        {
            StatusChange(1);
        }

        private void Image_MouseUp_1(object sender, MouseButtonEventArgs e)//2
        {
            int oldstat = selected.status;
            StatusChange(2);
            Actual ac = new Actual(selected);
            ac.ShowDialog();
            if (ac.DialogResult.Value)
            {

            }
            else
            {
                StatusChange(oldstat);
                stateVisible();
            }
            myTasks.Clear();
            myTasks.AddRange(Tasks.GetData());
            Refresh(Tasks.SortAndReturn(myTasks));
        }

        private void Image_MouseUp_2(object sender, MouseButtonEventArgs e)//0
        {
            StatusChange(0);
        }

        private void StatusChange(int state)
        {
            if (state == 0)
            {
                selected.status = 0;
                p1.Visibility = Visibility.Visible;
                p2.Visibility = Visibility.Hidden;
                p3.Visibility = Visibility.Hidden;
            }
            if (state == 1)
            {
                selected.status = 1;
                p1.Visibility = Visibility.Hidden;
                p2.Visibility = Visibility.Visible;
                p3.Visibility = Visibility.Hidden;
            }
            if (state == 2)
            {
                selected.status = 2;
                p1.Visibility = Visibility.Hidden;
                p2.Visibility = Visibility.Hidden;
                p3.Visibility = Visibility.Visible;
                 stateInvisible();
            }

            Tasks.EditTask(selected.ID, state);

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MyTask.Connection.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (selected == null)
            {
                MessageBox.Show("Please select an item!");
            }
            else
            {
                Edit edit = new Edit(selected.ID);
                edit.ShowDialog();
                if (edit.DialogResult.Value)
                {
                    myTasks.Clear();
                    myTasks.AddRange(Tasks.GetData());
                    Refresh(Tasks.SortAndReturn(myTasks));
                    stateInvisible();
                }

            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Stats stats = new Stats();
            stats.ShowDialog();
            stateInvisible();
        }

    }

}
