﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyTask;

namespace TimeToolWPF
{
    /// <summary>
    /// Interaction logic for Edit.xaml
    /// </summary>
    public partial class Edit : Window
    {
        int id = 0;
        Tasks task;
        public Edit(int id)
        {
            this.id = id;
            InitializeComponent();
            task = Tasks.GetData(id);
            tb_name.Text = task.name;
            tb_desc.Text = task.desc;
            if (task.importance)
                cb_imp.SelectedIndex = 1;
            else
                cb_imp.SelectedIndex = 0;
            if (task.urgency)
                cb_urg.SelectedIndex = 1;
            else
                cb_urg.SelectedIndex = 0;
            dt_plan.SelectedDate = task.planEnd;
            tb_dur.Text = task.planDuration.ToString();

            dt_plan.DisplayDateStart = DateTime.Now;
        }

        private void abort_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if((tb_name.Text == "")||(tb_desc.Text == ""))
            {
                throw new Exception();
            }
            DialogResult = true;
            task.name = tb_name.Text;
            task.desc = tb_desc.Text;
            task.importance = true;
            task.urgency = true;
            if (cb_imp.SelectedIndex == 0)
                task.importance = false;

            if (cb_urg.SelectedIndex == 0)
                task.urgency = false;
            task.planEnd = dt_plan.SelectedDate.Value;
            int dur;
            if (!int.TryParse(tb_dur.Text, out dur))
            {
                throw new Exception();
            }
            
            task.planDuration = dur;

            Tasks.SaveInDB(task, true);
            }
            catch
            {
                MessageBox.Show("You have to insert some values!");
            }
            this.Close();
            
        }
    }
}
