﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyTask;

namespace TimeToolWPF
{
    /// <summary>
    /// Interaction logic for Stats.xaml
    /// </summary>
    public partial class Stats : Window
    {
        List<Tasks> tasks = new List<Tasks>();
        double[] stats;
        public Stats()
        {
            InitializeComponent();
            stats = Tasks.GetStats();
            label5.Content = Math.Round(stats[0], 2).ToString() + " %";
            label6.Content = Math.Round(stats[1], 2).ToString() + " %";
           label7.Content = Math.Round(stats[2], 2).ToString() + " h";
            label8.Content = Math.Round(stats[3], 2).ToString() + " d";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                dataGridView1.ItemsSource = Tasks.GetData(start.SelectedDate.Value, end.SelectedDate.Value);
                dataGridView1.Columns.RemoveAt(0);
                dataGridView1.Columns.RemoveAt(4);
            }
            catch
            {
                MessageBox.Show("Please enter date!");

            }
            //dataGridView1.Columns.Remove("ID");
            //dataGridView1.Columns.Remove("status");
           // dataGridView1.ReadOnly = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            tasks.Clear();
            
            dataGridView1.ItemsSource= Tasks.GetData(Convert.ToDateTime("01.01.2001"), Convert.ToDateTime("01.01.2020"));
            dataGridView1.Columns.RemoveAt(0);
            dataGridView1.Columns.RemoveAt(4);
          
        }
    }
}
