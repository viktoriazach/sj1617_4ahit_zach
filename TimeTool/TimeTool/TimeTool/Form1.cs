﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using MyTask;

namespace TimeTool
{
    public partial class Form1 : Form
    {
        bool unselect = true;
        List<Tasks> myTasks;
        Tasks selected;
        public Form1()
        {
            InitializeComponent();
            myTasks = Tasks.GetData();
            Refresh(Tasks.SortAndReturn(myTasks));
            stateInvisible();
            button1.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Add add = new Add(myTasks);
            if(add.ShowDialog() == DialogResult.OK)
            {
                Refresh(Tasks.SortAndReturn(myTasks));
                stateInvisible();
            }
          

        }

        private void Refresh(List<Tasks>[] sorted)
        {
            listBox0.Items.Clear();
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();

            listBox0.Items.AddRange(sorted[0].ToArray());
            listBox1.Items.AddRange(sorted[1].ToArray());
            listBox2.Items.AddRange(sorted[2].ToArray());
            listBox3.Items.AddRange(sorted[3].ToArray());

        }

       

        private void UnSelectOthers(int i)
        {
            if (unselect)
            {
                switch (i)
                {
                    case 0:
                        unselect = false;
                        listBox1.SelectedIndex = -1;
                        listBox2.SelectedIndex = -1;
                        listBox3.SelectedIndex = -1;
                        unselect = true;
                        stateInvisible();
                        break;
                    case 1:
                        unselect = false;
                        listBox0.SelectedIndex = -1;
                        listBox2.SelectedIndex = -1;
                        listBox3.SelectedIndex = -1;
                        unselect = true;
                        stateInvisible();
                        break;
                    case 2:
                        unselect = false;
                        listBox0.SelectedIndex = -1;
                        listBox1.SelectedIndex = -1;
                        listBox3.SelectedIndex = -1;
                        unselect = true;
                        stateInvisible();
                        break;
                    case 3:
                        unselect = false;
                        listBox0.SelectedIndex = -1;
                        listBox1.SelectedIndex = -1;
                        listBox2.SelectedIndex = -1;
                        stateInvisible();
                        unselect = true;
                        break;
                }
            }
        }

        private void stateInvisible()
        {
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            label1.Visible = false;
            Waiting.Visible = false;
            working.Visible = false;
            Done.Visible = false;
        }

        private void stateVisible()
        {
            pictureBox2.Visible = true;
            pictureBox3.Visible = true;
            pictureBox4.Visible = true;
            label1.Visible = true;
            if (selected.status == 0)
                Waiting.Visible = true;
            if (selected.status == 1)
                working.Visible = true;
            if (selected.status == 2)
                Done.Visible = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

       

        private void b_edit_Click(object sender, EventArgs e)
        {
            if (selected == null)
            {
                MessageBox.Show("Please select an item!");
            }
            else
            {
                Edit edit = new Edit(selected.ID);
               
                if (DialogResult.OK == edit.ShowDialog())
                {
                    myTasks.Clear();
                    myTasks.AddRange(Tasks.GetData());
                    Refresh(Tasks.SortAndReturn(myTasks));
                    stateInvisible();
                }
               
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            StatusChange(1);
        }

        private void listBox0_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox0.SelectedItem == null)
                return;
            if (listBox0.SelectedIndex != -1)
                selected = (Tasks)listBox0.SelectedItem;
            UnSelectOthers(0);
           stateVisible();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
                return;
            if (listBox1.SelectedIndex != -1)
            selected = (Tasks)listBox1.SelectedItem;
            UnSelectOthers(1);
            stateVisible();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem == null)
                return;
            if (listBox2.SelectedIndex != -1)
            selected = (Tasks)listBox2.SelectedItem;
            UnSelectOthers(2);
            stateVisible();
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox3.SelectedItem == null)
                return;
            if (listBox3.SelectedIndex != -1)
            selected = (Tasks)listBox3.SelectedItem;
            UnSelectOthers(3);
           stateVisible();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StatusChange(0);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            int oldstat = selected.status;
            StatusChange(2);
            Actual ac = new Actual(selected);
            if(DialogResult.OK == ac.ShowDialog())
            {

            }
            else
            {
                StatusChange(oldstat);
                stateVisible();
            }
            myTasks.Clear();
            myTasks.AddRange(Tasks.GetData());
            Refresh(Tasks.SortAndReturn(myTasks));
        }

        private void StatusChange(int state)
        {
            if (state == 0)
            {
                selected.status = 0;
                Waiting.Visible = true;
                working.Visible = false;
                Done.Visible = false;
            }
            if (state == 1)
            {
                selected.status = 1;
                Waiting.Visible = false;
                working.Visible = true;
                Done.Visible = false;
            }
            if (state == 2)
            {
                selected.status = 2;
                Waiting.Visible = false;
                working.Visible = false;
                Done.Visible = true;
               stateInvisible();
            }

            Tasks.EditTask(selected.ID, state);

        }

        private void closing(object sender, FormClosingEventArgs e)
        {
            MyTask.Connection.Close();
        }

        private void b_archive_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < myTasks.Count(); i++ )
            {
                if (myTasks[i].status == 2)
                    myTasks.Remove(myTasks[i]);
                Refresh(Tasks.SortAndReturn(myTasks));
            }
            
        }

        private void b_info_Click(object sender, EventArgs e)
        {
            Info info = new Info();
            info.ShowDialog();

        }

        private void b_stats_Click(object sender, EventArgs e)
        {
            Stats stats = new Stats();
            stats.ShowDialog();
            stateInvisible();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();

            cmd.CommandText = "delete from TimeData where ID > 25";
            cmd.ExecuteScalar();
        }
    }

}
