﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyTask;

namespace TimeTool
{
    public partial class Add : Form
    {
       public List<Tasks> myt;
        public Add(List<Tasks> myt)
        {
            InitializeComponent();
           
            this.myt = myt;
            d_end.MinDate = DateTime.Now;

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                bool imp = true;
                bool urg = true;

                this.DialogResult = DialogResult.OK;
                if (c_importance.SelectedItem.ToString() == "low")
                    imp = false;

                if (c_urgency.SelectedItem.ToString() == "low")
                    urg = false;

                myt.Add(new Tasks(Tasks.GetIndex(), t_name.Text, t_desc.Text, imp, urg, Convert.ToDateTime(d_end.Text), Convert.ToInt32(n_duration.Value)));
            }
            catch(NullReferenceException ex)
            {
                MessageBox.Show("You have to insert some values!");
            }
            this.Close();
        }
    }
}
