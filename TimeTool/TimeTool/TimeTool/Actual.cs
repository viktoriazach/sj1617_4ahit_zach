﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyTask;

namespace TimeTool
{
    public partial class Actual : Form
    {
        Tasks task;
        public Actual(Tasks task)
        {
            this.task = task;
            InitializeComponent();
            d_end.MaxDate = DateTime.Now;
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            task.actDuration = Convert.ToInt32(n_duration.Value);
            task.actEnd = d_end.Value;
            Tasks.SaveInDB(task, true);
        }

        private void d_end_ValueChanged(object sender, EventArgs e)
        {

        }

    }
}
