﻿namespace TimeTool
{
    partial class Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.t_name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.c_importance = new System.Windows.Forms.ComboBox();
            this.c_urgency = new System.Windows.Forms.ComboBox();
            this.d_end = new System.Windows.Forms.DateTimePicker();
            this.t_desc = new System.Windows.Forms.TextBox();
            this.n_duration = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.n_duration)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "ADD NEW TASK";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Importance";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Urgency";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 25);
            this.label5.TabIndex = 4;
            this.label5.Text = "Planned End";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(162, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Planned Duration";
            // 
            // t_name
            // 
            this.t_name.Location = new System.Drawing.Point(275, 87);
            this.t_name.Name = "t_name";
            this.t_name.Size = new System.Drawing.Size(285, 29);
            this.t_name.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(66, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 25);
            this.label7.TabIndex = 7;
            this.label7.Text = "Name";
            // 
            // c_importance
            // 
            this.c_importance.FormattingEnabled = true;
            this.c_importance.Items.AddRange(new object[] {
            "high",
            "low"});
            this.c_importance.Location = new System.Drawing.Point(275, 226);
            this.c_importance.Name = "c_importance";
            this.c_importance.Size = new System.Drawing.Size(285, 32);
            this.c_importance.TabIndex = 8;
            // 
            // c_urgency
            // 
            this.c_urgency.FormattingEnabled = true;
            this.c_urgency.Items.AddRange(new object[] {
            "high",
            "low"});
            this.c_urgency.Location = new System.Drawing.Point(275, 269);
            this.c_urgency.Name = "c_urgency";
            this.c_urgency.Size = new System.Drawing.Size(285, 32);
            this.c_urgency.TabIndex = 9;
            // 
            // d_end
            // 
            this.d_end.Location = new System.Drawing.Point(275, 308);
            this.d_end.Name = "d_end";
            this.d_end.Size = new System.Drawing.Size(285, 29);
            this.d_end.TabIndex = 10;
            // 
            // t_desc
            // 
            this.t_desc.Location = new System.Drawing.Point(247, 90);
            this.t_desc.MaxLength = 200;
            this.t_desc.Multiline = true;
            this.t_desc.Name = "t_desc";
            this.t_desc.Size = new System.Drawing.Size(285, 86);
            this.t_desc.TabIndex = 7;
            // 
            // n_duration
            // 
            this.n_duration.Location = new System.Drawing.Point(275, 348);
            this.n_duration.Name = "n_duration";
            this.n_duration.Size = new System.Drawing.Size(245, 29);
            this.n_duration.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(532, 348);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 25);
            this.label8.TabIndex = 13;
            this.label8.Text = "h";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(259, 418);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(301, 44);
            this.button1.TabIndex = 14;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(70, 418);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 44);
            this.button2.TabIndex = 15;
            this.button2.Text = "ABORT";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(202)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(641, 485);
            this.panel1.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.t_desc);
            this.panel2.Location = new System.Drawing.Point(16, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(603, 444);
            this.panel2.TabIndex = 0;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 509);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.n_duration);
            this.Controls.Add(this.d_end);
            this.Controls.Add(this.c_urgency);
            this.Controls.Add(this.c_importance);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.t_name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Add";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimeTool";
            ((System.ComponentModel.ISupportInitialize)(this.n_duration)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox t_name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox c_importance;
        private System.Windows.Forms.ComboBox c_urgency;
        private System.Windows.Forms.DateTimePicker d_end;
        private System.Windows.Forms.TextBox t_desc;
        private System.Windows.Forms.NumericUpDown n_duration;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}