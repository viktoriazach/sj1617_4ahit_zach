﻿namespace TimeTool
{
    partial class Stats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stats));
            this.timeToolDataSet = new TimeTool.TimeToolDataSet();
            this.timeDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.timeDataTableAdapter = new TimeTool.TimeToolDataSetTableAdapters.TimeDataTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.start = new System.Windows.Forms.DateTimePicker();
            this.end = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.l_intime = new System.Windows.Forms.Label();
            this.l_delayed = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.l_avgdelay = new System.Windows.Forms.Label();
            this.l_avgtime = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.timeToolDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // timeToolDataSet
            // 
            this.timeToolDataSet.DataSetName = "TimeToolDataSet";
            this.timeToolDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // timeDataBindingSource
            // 
            this.timeDataBindingSource.DataMember = "TimeData";
            this.timeDataBindingSource.DataSource = this.timeToolDataSet;
            // 
            // timeDataTableAdapter
            // 
            this.timeDataTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(202)))), ((int)(((byte)(255)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(21, 143);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 31;
            this.dataGridView1.Size = new System.Drawing.Size(1692, 481);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(189, 33);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(311, 29);
            this.start.TabIndex = 1;
            this.start.ValueChanged += new System.EventHandler(this.start_ValueChanged);
            // 
            // end
            // 
            this.end.Location = new System.Drawing.Point(189, 86);
            this.end.Name = "end";
            this.end.Size = new System.Drawing.Size(311, 29);
            this.end.TabIndex = 2;
            this.end.ValueChanged += new System.EventHandler(this.end_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(527, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(182, 42);
            this.button1.TabIndex = 3;
            this.button1.Text = "SHOW";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Stats between";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "and";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1204, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 25);
            this.label3.TabIndex = 6;
            this.label3.Text = "In Time:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1204, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Delayed:";
            // 
            // l_intime
            // 
            this.l_intime.AutoSize = true;
            this.l_intime.Location = new System.Drawing.Point(1311, 33);
            this.l_intime.Name = "l_intime";
            this.l_intime.Size = new System.Drawing.Size(64, 25);
            this.l_intime.TabIndex = 8;
            this.l_intime.Text = "label5";
            // 
            // l_delayed
            // 
            this.l_delayed.AutoSize = true;
            this.l_delayed.Location = new System.Drawing.Point(1311, 82);
            this.l_delayed.Name = "l_delayed";
            this.l_delayed.Size = new System.Drawing.Size(64, 25);
            this.l_delayed.TabIndex = 9;
            this.l_delayed.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(975, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 25);
            this.label7.TabIndex = 10;
            this.label7.Text = "OVERALL STATS:";
            // 
            // l_avgdelay
            // 
            this.l_avgdelay.AutoSize = true;
            this.l_avgdelay.Location = new System.Drawing.Point(1527, 82);
            this.l_avgdelay.Name = "l_avgdelay";
            this.l_avgdelay.Size = new System.Drawing.Size(64, 25);
            this.l_avgdelay.TabIndex = 14;
            this.l_avgdelay.Text = "label8";
            // 
            // l_avgtime
            // 
            this.l_avgtime.AutoSize = true;
            this.l_avgtime.Location = new System.Drawing.Point(1527, 33);
            this.l_avgtime.Name = "l_avgtime";
            this.l_avgtime.Size = new System.Drawing.Size(64, 25);
            this.l_avgtime.TabIndex = 13;
            this.l_avgtime.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1403, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 25);
            this.label10.TabIndex = 12;
            this.label10.Text = "Avg. Delay:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1403, 33);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 25);
            this.label11.TabIndex = 11;
            this.label11.Text = "Avg. Time:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(527, 81);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(182, 42);
            this.button2.TabIndex = 15;
            this.button2.Text = "SHOW ALL";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Stats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(168F, 168F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1731, 648);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.l_avgdelay);
            this.Controls.Add(this.l_avgtime);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.l_delayed);
            this.Controls.Add(this.l_intime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.end);
            this.Controls.Add(this.start);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Stats";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimeTool";
            this.Load += new System.EventHandler(this.Stats_Load);
            ((System.ComponentModel.ISupportInitialize)(this.timeToolDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TimeToolDataSet timeToolDataSet;
        private System.Windows.Forms.BindingSource timeDataBindingSource;
        private TimeToolDataSetTableAdapters.TimeDataTableAdapter timeDataTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker start;
        private System.Windows.Forms.DateTimePicker end;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label l_intime;
        private System.Windows.Forms.Label l_delayed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label l_avgdelay;
        private System.Windows.Forms.Label l_avgtime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
    }
}