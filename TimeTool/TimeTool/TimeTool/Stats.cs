﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyTask;

namespace TimeTool
{
    public partial class Stats : Form
    {
        double[] stats;
        public Stats()
        {
            InitializeComponent();
            stats = Tasks.GetStats();
            l_intime.Text = Math.Round(stats[0],2).ToString()+" %";
            l_delayed.Text = Math.Round(stats[1],2).ToString()+" %";
            l_avgtime.Text= Math.Round(stats[2],2).ToString()+" h";
            l_avgdelay.Text = Math.Round(stats[3],2).ToString()+" d";
        }

        private void Stats_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'timeToolDataSet.TimeData' table. You can move, or remove it, as needed.
            //this.timeDataTableAdapter.Fill(this.timeToolDataSet.TimeData);

            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Tasks.GetData(start.Value, end.Value);
           dataGridView1.Columns.Remove("ID");
            dataGridView1.Columns.Remove("status");
            dataGridView1.ReadOnly = true;
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void start_ValueChanged(object sender, EventArgs e)
        {

        }

        private void end_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Tasks.GetData(Convert.ToDateTime("01.01.2001"), Convert.ToDateTime("01.01.2020"));
            dataGridView1.Columns.Remove("ID");
            dataGridView1.Columns.Remove("status");
            dataGridView1.ReadOnly = true;
        }
    }
}
