﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyTask;

namespace TimeTool
{
    public partial class Edit : Form
    {
        int id = 0;
        Tasks task;
        public Edit(int id)
        {
            this.id = id;
            InitializeComponent();
            task = Tasks.GetData(id);
            t_name.Text = task.name;
            t_desc.Text = task.desc;
            if (task.importance)
                c_importance.SelectedIndex = 0;
            else
                c_importance.SelectedIndex = 1;
            if (task.urgency)
                c_urgency.SelectedIndex = 0;
            else
                c_urgency.SelectedIndex = 1;
            d_end.Value = task.planEnd;
            n_duration.Value = task.planDuration;

            d_end.MinDate = DateTime.Now;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            task.name = t_name.Text;
            task.desc = t_desc.Text;
           task.importance=true;
            task.urgency = true;
            if (c_importance.SelectedItem.ToString() == "low")
                task.importance = false;
              if (c_urgency.SelectedItem.ToString() == "low")
                task.urgency = false;

              task.planEnd = d_end.Value;
              task.planDuration = Convert.ToInt32(n_duration.Value);

            Tasks.SaveInDB(task, true);
            this.Close();
          //  c_importance = (task.importance)?(c_importance.SelectedItem.ToString()="high"):(c_importance.SelectedItem="low");
         //   urg, Convert.ToDateTime(d_end.Text), Convert.ToInt32(n_duration.Value))
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }
    }
}
