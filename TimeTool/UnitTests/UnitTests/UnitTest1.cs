﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyTask;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        //DB auf Ursprung zurücksetzen beim Starten - die ersten 10 Einträge beibehalten

        [TestMethod]
        public void TestDBreading()
        {
            List<Tasks> mytasks = Tasks.GetData();
            Assert.AreEqual(Convert.ToInt32(7), Convert.ToInt32(mytasks.Count));
        }

        [TestMethod]
        public void IndexMethod()
        {
            int index = Tasks.GetIndex();
            Assert.AreEqual(52236+1, index);
        }

        [TestMethod]
        public void TestDBwriting()
        {
            int anz = Tasks.GetData().Count+1;
            new Tasks(1, "test", "desc", true, true, DateTime.Now, 2);
            int anz2 = Tasks.GetData().Count;
            Assert.AreEqual(anz, anz2);
            OleDbConnection con = MyTask.Connection.GetConnection();
        }

        [TestMethod]
        public void Sorting()
        {
            List<Tasks>[] feld = Tasks.SortAndReturn(Tasks.GetData());
            Assert.AreEqual(Tasks.GetData().Count, feld[0].Count + feld[1].Count + feld[2].Count + feld[3].Count);
        }
        


    }
}
