﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;


namespace MyTask
{
    public class Tasks
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public bool importance { get; set; }
        public bool urgency { get; set; }
        public int status { get; set; }     // 0-new / 1-working / 2-ready
        public DateTime planEnd { get; set; }
        public DateTime actEnd { get; set; }
        public int planDuration { get; set; }
        public int actDuration { get; set; }
        public bool delayed { get; set; }

        public Tasks(int id, string n, string d, bool i, bool u, DateTime plan, int dur)
        {
            ID = id;
            name = n;
            desc = d;
            importance = i;
            urgency = u;
            status = 0;
            planEnd = plan;
            planDuration = dur;
            SaveInDB(this, false);
        }

        public Tasks(string n, string d, bool i, bool u, DateTime plan, int dur, DateTime act, int acdur)
        {
            name = n;
            desc = d;
            importance = i;
            urgency = u;
            status = 0;
            planEnd = plan;
            planDuration = dur;
            actEnd = act;
            actDuration = acdur;
            if (planEnd > actEnd)
                delayed = false;
            else
                delayed = true;
            // SaveInDB(this, false);
        }

        public Tasks(int id, string n, string d, bool i, bool u, int s, DateTime plan, int dur, DateTime act, int adur)
        {
            ID = id;
            name = n;
            desc = d;
            importance = i;
            urgency = u;
            status = 0;
            planEnd = plan;
            planDuration = dur;
            status = s;
            actEnd = act;
            actDuration = adur;

        }

        static public void SaveInDB(Tasks my, bool update)
        {

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();

            if (!update)
            {
                string end = "\"" + my.planEnd.ToShortDateString() + "\"";
                string act = "\"" + my.actEnd.ToShortDateString() + "\"";
                //MessageBox.Show("insert into TimeData (nam,desc,importance,urgency, status, planEnd,planTime,actEnd, actTime) values('" + item.name + "', '" + item.desc + "', " + item.importance + ", " + item.urgency + ", " + item.status + ", " + end + ", " + item.planDuration + ", " + act + ", " + item.actDuration + ");");
                cmd.CommandText = "insert into TimeData (nam,descript,importance,urgency, status, planEnd,planTime,actEnd, actTime) values(\"" + my.name + "\", \"" + my.desc + "\", " + my.importance + ", " + my.urgency + ", " + my.status + ", " + end + ", " + my.planDuration + ", " + act + ", " + my.actDuration + ");";
                //cmd.CommandText = "select * from TimeData";
                cmd.ExecuteNonQuery();
                /*cmd.CommandText = "select ID from TimeData where nam ='" + this.name + "' and descript = '" + this.descript + "';";
                this.id = Convert.ToInt32(cmd.ExecuteScalar());*/
            }

            if (update)
            {
                /*cmd.CommandText = "UPDATE taböhn SET actEnd = @actEnd";

                OleDbParameter actEnd = cmd.CreateParameter();
                actEnd.ParameterName = "@actEnd";
                actEnd.Value = my.actEnd;
                cmd.Parameters.Add(actEnd);*/

                cmd.CommandText = "update TimeData set actEnd = \"" + my.actEnd + "\", actTime = " + my.actDuration + ", nam = '" + my.name + "', descript = '" + my.desc + "', importance = " + my.importance + ", urgency = " + my.urgency + ", planEnd = \"" + my.planEnd + "\", planTime = " + my.planDuration + " where id = " + my.ID + ";";
                cmd.ExecuteNonQuery();
            }

        }

        static public List<Tasks> GetData()
        {
            List<Tasks> mytasks = new List<Tasks>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();
            cmd.CommandText = "select id, nam,descript,importance,urgency, status, planEnd,planTime,actEnd, actTime from TimeData where status <> 2";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                mytasks.Add(new Tasks(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetBoolean(3), reader.GetBoolean(4), reader.GetInt32(5), reader.GetDateTime(6), reader.GetInt32(7), reader.GetDateTime(8), reader.GetInt32(9)));
            }
            return mytasks;
        }

        static public List<Tasks> GetData(DateTime start, DateTime end)
        {
            List<Tasks> mytasks = new List<Tasks>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();
            cmd.CommandText = "select nam,descript, importance, urgency, planEnd, planTime, actEnd, actTime from TimeData where planEnd between #" + start.ToString("MM'/'dd'/'yyyy") + "# and #" + end.ToString("MM'/'dd'/'yyyy") + "# and status = 2";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                mytasks.Add(new Tasks(reader.GetString(0), reader.GetString(1), reader.GetBoolean(2), reader.GetBoolean(3), reader.GetDateTime(4), reader.GetInt32(5), reader.GetDateTime(6), reader.GetInt32(7)));
            }
            return mytasks;
        }

        static public Tasks GetData(int id)
        {

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();
            cmd.CommandText = "select ID, nam,descript,importance,urgency, status, planEnd,planTime,actEnd, actTime from TimeData WHERE ID = " + id + ";";
            // cmd.CommandText = "select * from TimeData WHERE ID = 6";
            OleDbDataReader reader = cmd.ExecuteReader();
            reader.Read();
            /* MessageBox.Show(reader.GetInt32(0)+"");
             MessageBox.Show(reader.GetString(1) + "");
             MessageBox.Show(reader.GetString(2) + "");
             MessageBox.Show(reader.GetBoolean(3) + "");
             MessageBox.Show(reader.GetBoolean(4) + "");
             MessageBox.Show(reader.GetInt32(5) + "");
             MessageBox.Show(reader.GetDateTime(6) + "");
             MessageBox.Show(reader.GetInt32(7) + "");
             MessageBox.Show(reader.GetDateTime(8) + "");
             MessageBox.Show(reader.GetInt32(9) + "");*/

            return new Tasks(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetBoolean(3), reader.GetBoolean(4), reader.GetInt32(5), reader.GetDateTime(6), reader.GetInt32(7), reader.GetDateTime(8), reader.GetInt32(9));
        }

        static public void EditTask(int id, int state)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();
            //cmd.CommandText = "insert into TimeData (nam,descript,importance,urgency, status, planEnd,planTime,actEnd, actTime) values(\"" + name + "\", \"" + desc + "\", " + importance + ", " + urgency + ", " + status + ", " + end + ", " + planDuration + ", " + act + ", " + actDuration + ");";
            cmd.CommandText = "update TimeData set status = " + state + " where id = " + id;
            cmd.ExecuteNonQuery();
        }

        public static List<Tasks>[] SortAndReturn(List<Tasks> myt)
        {
            //start left bottom and go not clockwise (like in Maths - A0 B1 C2 D3)

            List<Tasks>[] sorted = new List<Tasks>[4];
            sorted[0] = new List<Tasks>();
            sorted[1] = new List<Tasks>();
            sorted[2] = new List<Tasks>();
            sorted[3] = new List<Tasks>();
            foreach (Tasks item in myt)
            {
                if (!item.importance && !item.urgency)
                    sorted[0].Add(item);
                if (!item.importance && item.urgency)
                    sorted[1].Add(item);
                if (item.importance && item.urgency)
                    sorted[2].Add(item);
                if (item.importance && !item.urgency)
                    sorted[3].Add(item);
            }

            return sorted;
        }

        public override string ToString()
        {
            string first20 = "";
            if (name.Length > 20)
            {
                for (int i = 0; i < 20; i++)
                {
                    first20 += name[i];
                }
            }
            else{first20 = name;}
            return " >  " + first20;
        }

        public static double[] GetStats()
        {
            double[] stats = new double[4];
            List<Tasks> mytasks = new List<Tasks>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();
            cmd.CommandText = "select id, nam,descript,importance,urgency, status, planEnd,planTime,actEnd, actTime from TimeData where status = 2";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                mytasks.Add(new Tasks(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetBoolean(3), reader.GetBoolean(4), reader.GetInt32(5), reader.GetDateTime(6), reader.GetInt32(7), reader.GetDateTime(8), reader.GetInt32(9)));
            }

            int early = 0;
            int durationac = 0;
            double dely = 0;
            foreach (Tasks item in mytasks)
            {
                durationac += item.actDuration;

                if (item.planEnd > item.actEnd)
                {
                    early++;
                }
                else
                {
                    dely += (item.actEnd - item.planEnd).TotalDays;
                }
            }
            double e = Convert.ToDouble(early);
            double c = Convert.ToDouble(mytasks.Count);
            stats[0] = Convert.ToInt32((e / c) * 100);
            stats[1] = 100 - stats[0];
            stats[2] = durationac / mytasks.Count;
            stats[3] = dely / (mytasks.Count - early);


            return stats;
        }

        public static int GetIndex()
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = Connection.GetConnection();
            cmd.CommandText = "select MAX(ID) from TimeData";
            return Convert.ToInt32(cmd.ExecuteScalar()) + 1;
        }

    }

}
