﻿/*
 * Name: Connection.cs
 * Autor: Viktoria Zach
 * Datum: 2.5.2016
 * Beschreibung: Diese Klasse dient dazu, dass die OleDbConnection zurückgegeben wird.
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace MyTask
{
    public class Connection
    {
        static OleDbConnection con = null;
        static Connection c = null;
        private Connection()
        {
            string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=TimeTool.mdb";

            con = new OleDbConnection(connectionString);
        }

        public static void Close()
        {
            con.Close();
        }

        public static OleDbConnection GetConnection()
        {
            if (c == null)
            {
                try
                {
                    c = new Connection();
                    con.Open();
                }
                catch
                {
                    //MessageBox.Show("Uuups! Da ging etwas schief.\n\n    >Connection fehlgeschlagen<");
                }
            }
            return con;
           
        }

    }
}
