﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI
{
    public partial class Form1 : Form
    {

        Pen[] pens = new Pen[20];
        Point[] points = new Point[20];
        Random rand = new Random();
        Point zero = new Point(0, 0);
        int durchlauf = 0;
        public Form1()
        {
            this.StartPosition = 0;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
            timer1.Start();
        }

        //private void Exercise_Paint(object sender, PaintEventArgs e)
        //{
            
        //}
        
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if ((durchlauf > 0)&&(durchlauf < 20))
            {
                e.Graphics.DrawLine(pens[0], zero, points[0]);

                for (int i = 1; i < durchlauf; i++)
                {
                    e.Graphics.DrawLine(pens[i], points[i - 1], points[i]);
                }
            }
            else if(durchlauf >= 20)
            {
                e.Graphics.DrawLine(pens[0], zero, points[0]);

                for (int i = 1; i < 19; i++)
                {
                    e.Graphics.DrawLine(pens[i], points[i - 1], points[i]);
                }
            }
          //  Invalidate
            // ------------- Hausi --------------------
            /*
            Pen penCurrent = new Pen(Color.Yellow, 5.5F);
            //e.Graphics.DrawLine(penCurrent, 100, 20, 205, 20);
            
            Point[] points = new Point[5];
            points[0] = new Point(270, 70);
             points[1] = new Point(270, 270);
             points[2] = new Point(70, 270);
             points[3] = new Point(70, 70);
             points[4] = new Point(270, 70);
             e.Graphics.DrawLines(penCurrent, points);



             
            penCurrent = new Pen(Color.Red, 5.5F);
             points = new Point[3];
             points[0] = new Point(70, 70);
             points[1] = new Point(170, 20);
             points[2] = new Point(270, 70);
             e.Graphics.DrawLines(penCurrent, points);*/
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int zahl = rand.Next();
            if (durchlauf < pens.Length)
            {
                pens[durchlauf] = new Pen(Color.FromArgb(zahl), 4F);
                points[durchlauf] = new Point(rand.Next(0, this.Width), rand.Next(0, this.Height));
            }
            else
            {
                for (int i = 0; i < pens.Length-1; i++)
                {
                    pens[i] = (Pen)pens[i + 1].Clone();
                    points[i] = new Point(points[i + 1].X, points[i + 1].Y);
                }
                pens[pens.Length-1] = new Pen(Color.FromArgb(zahl), 4F);
                points[pens.Length-1] = new Point(rand.Next(0, this.Width), rand.Next(0, this.Height));
            }
            durchlauf++;
            Invalidate();
        }
    }
}
