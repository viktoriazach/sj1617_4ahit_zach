This is ApacheBench, Version 2.3 <$Revision: 1663405 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)


Server Software:        Apache/2.4.16
Server Hostname:        127.0.0.1
Server Port:            80

Document Path:          /index.html
Document Length:        116 bytes

Concurrency Level:      500
Time taken for tests:   136.731 seconds
Complete requests:      10000
Failed requests:        8
   (Connect: 8, Receive: 0, Length: 0, Exceptions: 0)
Total transferred:      3880000 bytes
HTML transferred:       1160000 bytes
Requests per second:    73.14 [#/sec] (mean)
Time per request:       6836.555 [ms] (mean)
Time per request:       13.673 [ms] (mean, across all concurrent requests)
Transfer rate:          27.71 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   12  99.4      1    1314
Processing:    64 6755 948.8   6860    9292
Waiting:       64 4483 1407.0   4448    8151
Total:         64 6767 948.7   6864    9293

Percentage of the requests served within a certain time (ms)
  50%   6864
  66%   7069
  75%   7174
  80%   7276
  90%   7626
  95%   7897
  98%   8140
  99%   8232
 100%   9293 (longest request)
