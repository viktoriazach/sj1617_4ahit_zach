﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Server
{
    class ResponseFactory
    {

        public static string GetResponse(string request, ref bool isPic)
        {
            StreamReader sr;

            string filename = request.Split('/')[1];
            filename = filename.Split(' ')[0];
           // Console.Write(filename);

            if(filename == "")
            {
                isPic = false;
                sr = new StreamReader("index.html");
                Header h = new Header("index.html");
                return h.ToString() + sr.ReadToEnd();
            }

            if(filename.IndexOf("html") != -1)
            {
                isPic = false;
                sr = new StreamReader(filename);
                Header h = new Header(filename);
                return h.ToString() + sr.ReadToEnd();
            }

            if ((filename.IndexOf("gif") != -1) || (filename.IndexOf("png") != -1) || (filename.IndexOf("jpg") != -1))
            {
                 isPic = true;
                 Header h = new Header(filename);
                 return h.ToString();
            }

            isPic = false;
            sr = new StreamReader("error.html");
            Header he = new Header("error.html");
            return he.ToString() + sr.ReadToEnd();

        }

    }
}
