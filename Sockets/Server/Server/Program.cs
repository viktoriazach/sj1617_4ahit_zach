﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Configuration;


namespace Server
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients
        static bool isPic = false;
        public static void Main()
        {
            listener = new TcpListener(IPAddress.Any, 8888);
            listener.Start();
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
               // Console.WriteLine("Connected: {0} ", soc.RemoteEndPoint);
                try
                {

                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    string requeststring = sr.ReadLine();
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true;

                    string response = ResponseFactory.GetResponse(requeststring, ref isPic);

                    sw.Write(response);

                    if (isPic)
                    {
                        string filename = requeststring.Split('/')[1];
                        filename = filename.Split(' ')[0];
                        soc.SendFile(filename);
                    }
                  
                    



                }
                catch (Exception e)
                {
                 //   Console.WriteLine(e.Message);
                }

                soc.Close();
            }
        }
    }
}

