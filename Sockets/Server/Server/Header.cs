﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Server
{
    class Header
    {

            DateTime date;
            public string host, type, version, status;

            public Header(string target)
            {
                if ((target.Split('.')[1].Substring(0, 3) == ("png")) || (target.Split('.')[1].Substring(0, 3) == ("jpg")) || (target.Split('.')[1].Substring(0, 3) == ("gif")))
                    type = "Content-Type: image/" + target.Split('.')[1].Substring(0, 3) + "\n";
                else
                type = "Content-Type: text/html\n";

                if(target == "error.html")
                {
                    status = "404 Not Found\n";
                }
                else
                status = "200 OK\n";

            }

            public override string ToString()
            {
                date = DateTime.Now;
                host = "MyServer\n";
                version = "HTTP/1.1\n";

                return version + status + type + "Date: " + Convert.ToString(date) + "\nServer: " + host + "Connection: closed\n\n";
            }
      }
}
