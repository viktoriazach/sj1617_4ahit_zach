﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace NanoCAD
{
    public partial class Form1 : Form
    {
        List<Shape> ToDraw = new List<Shape>();
        List<Shape> Intersects = new List<Shape>();
        List<Font> inters = new List<Font>();
        int zoom = 1;
        Shape selectedItem;
        bool sthselected = false;
        string actual;
        Color col = Color.Black;
        System.Drawing.Font fntWrite;

        public Form1()
        {

            InitializeComponent();
            allInvisible();
            p_rectangle.Left = p_line.Left;
            p_rectangle.Top = p_line.Top;
            p_circle.Top = p_line.Top;
            p_circle.Left = p_line.Left;
            SetValues();
            button_inter.Visible = false;

            button8.Visible = false;
            FontFamily fntFamily = new FontFamily("Times New Roman");
            fntWrite = new System.Drawing.Font(fntFamily, 10.00F, FontStyle.Regular);
        }

        private void SetValues()
        {
            line_x1.Maximum = panel1.Width;
            line_x2.Maximum = panel1.Width;
            circle_x1.Maximum = panel1.Width;
            rectangle_x1.Maximum = panel1.Width;
            rectangle_x2.Maximum = panel1.Width;

            line_y1.Maximum = panel1.Height;
            line_y2.Maximum = panel1.Height;
            circle_y1.Maximum = panel1.Height;
            rectangle_y1.Maximum = panel1.Height;
            rectangle_y2.Maximum = panel1.Height;

            rectangle_width.Maximum = panel1.Width;
            rectangle_height.Maximum = panel1.Height;
            }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Shape s in ToDraw)
            {
                if (zoom != 0)
                    e.Graphics.Transform = new Matrix(this.zoom, 0, 0, this.zoom, 0, 0);

                s.DrawMe(e);
                if (s.amIselected)
                    s.DrawMarkers(e);
            }
            
            foreach (Font f in inters)
            {
                e.Graphics.DrawString(f.Text, fntWrite, Brushes.Red, f.p);
            }
            foreach (Circle c in Intersects)
            {
                c.DrawMe(e);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            allInvisible();
            actual = listBox1.SelectedItem.ToString();
            if(listBox1.SelectedItem.ToString() == "Circle")
            {
                p_circle.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Rectangle")
            {
                p_rectangle.Visible = true;
            }
            if (listBox1.SelectedItem.ToString() == "Line")
            {
                p_line.Visible = true;
            }
        }

        private void allInvisible()
        {
            p_circle.Visible = false;
            p_rectangle.Visible = false;
            p_line.Visible = false;
        }
        //Draw
        private void button1_Click(object sender, EventArgs e)
        {
            Intersects.Clear();
            inters.Clear();
            if ((sthselected))
            {
               // MessageBox.Show(selectedItem.GetType().ToString());
                if ((selectedItem.GetType().ToString() == "NanoCAD."+actual))
                {
                    ToDraw.Remove(selectedItem);

                }
            }

            if (actual == "Circle")
            {
                
                ToDraw.Add(new Circle((int)circle_x1.Value, (int)circle_y1.Value, (int)circle_rad.Value, col, thick.Value));
                panel1.Invalidate(); 
            }
            if (actual == "Rectangle")
            {
                ToDraw.Add(new Rectangle((int)rectangle_x1.Value, (int)rectangle_y1.Value, (int)rectangle_x2.Value, (int)rectangle_y2.Value, true, col, thick.Value));

                panel1.Invalidate();
            }
            if (actual == "Line")
            {
                    ToDraw.Add(new Line((int)line_x1.Value, (int)line_y1.Value, (int)line_x2.Value, (int)line_y2.Value, col, thick.Value));
                    panel1.Invalidate();
            }
        }

        //Zurück Button
        private void button3_Click(object sender, EventArgs e)
        {
            Intersects.Clear();
            inters.Clear();
            if (ToDraw.Count != 0)
            {
                ToDraw[ToDraw.Count - 1].pen.Dispose();
                ToDraw.RemoveAt(ToDraw.Count - 1);
                panel1.Invalidate();
            }
        }

        //Alles löschen Button
        private void button2_Click(object sender, EventArgs e)
        {
            Intersects.Clear();
            inters.Clear();
            foreach (Shape i in ToDraw)
            {
                i.pen.Dispose();
            }
            ToDraw.Clear();
            panel1.Invalidate();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            SetValues();
        }

        private void rectangle_y1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void rectangle_y2_ValueChanged(object sender, EventArgs e)
        {
            rectangle_height.Value = Math.Abs(rectangle_y1.Value - rectangle_y2.Value);
        }

        private void line_x2_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void rectangle_x2_ValueChanged(object sender, EventArgs e)
        {
            rectangle_width.Value = Math.Abs(rectangle_x1.Value - rectangle_x2.Value);
        }

        private void rectangle_width_ValueChanged(object sender, EventArgs e)
        {
           
               // rectangle_x2.Value = Math.Abs(rectangle_x1.Value - rectangle_width.Value);
        }

        private void rectangle_height_ValueChanged(object sender, EventArgs e)
        {
            
               // rectangle_y2.Value = Math.Abs(rectangle_y1.Value - rectangle_height.Value);
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            
            Intersects.Clear();
            inters.Clear();
            button_inter.Visible = false;
            button8.Visible = false;
            if(zoom == 1)
            {
                Point coords = panel1.PointToClient(Cursor.Position);
                foreach (Shape s in ToDraw)
                    s.amIselected = false;

                sthselected = false;
                Rectangle area;
                foreach (Shape s in ToDraw)
                {

                    area = (Rectangle)s.ReturnArea();
                    if ((isBetween(area.top.X, area.top.X + area.width, coords.X)) && (isBetween(area.top.Y, area.top.Y + area.height, coords.Y)))
                    {
                        s.amIselected = true;
                        sthselected = true;
                        selectedItem = s;
                        if (s is Rectangle)
                        {
                            listBox1.SelectedItem = "Rectangle";
                            Rectangle r = (Rectangle)s;
                            rectangle_height.Value = r.height;
                            rectangle_width.Value = r.width;
                            rectangle_x1.Value = r.top.X;
                            rectangle_x2.Value = r.top.X + r.width;
                            rectangle_y1.Value = r.top.Y;
                            rectangle_y2.Value = r.top.Y + r.height;

                        }
                        if (s is Circle)
                        {
                            listBox1.SelectedItem = "Circle";
                            Circle c = (Circle)s;
                            circle_rad.Value = c.rad;
                            circle_x1.Value = c.middle.X;
                            circle_y1.Value = c.middle.Y;
                        }
                        if (s is Line)
                        {
                            listBox1.SelectedItem = "Line";
                            Line l = (Line)s;
                            line_x1.Value = l.start.X;
                            line_y1.Value = l.start.Y;
                            line_x2.Value = l.end.X;
                            line_y2.Value = l.end.Y;

                            button_inter.Visible = true;
                            button8.Visible = true;
                        }


                        break;
                    }

                }
                panel1.Invalidate();
            }
            else
            {
                MessageBox.Show("You cannot choose a shape while Zoom is active!");
             
            }
            
        }

        private bool isBetween(int w1, int w2, int check)
        {
            if ((w1 <= check) && (check <= w2))
                return true;

            return false;
            
        }

        //Zoom Button
        private void button4_Click(object sender, EventArgs e)
        {
            zoom = zoom * 2;
            panel1.Invalidate();
        }

        //Zoom Button
        private void button5_Click(object sender, EventArgs e)
        {
            zoom = zoom / 2;
            panel1.Invalidate();
        }

        //Ausgewähltes Löschen
        private void button6_Click(object sender, EventArgs e)
        {
           
            Intersects.Clear();
           
            inters.Clear();
            button_inter.Visible = false;
            button8.Visible = false;
            if (sthselected)
            {
                ToDraw.Remove(selectedItem);
                selectedItem.pen.Dispose();
                panel1.Invalidate();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                col = colorDialog1.Color;
            }
            button7.BackColor = col;
            pictureBox6.BackColor = col;
            if(col == Color.Black)
            {
                button7.ForeColor = Color.White;
            }
            else { button7.ForeColor = Color.Black; }
        }

        private void filename_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = ".ncad";
           // savedialog sv = new savedialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);
               // MessageBox.Show(saveFileDialog1.FileName);
               foreach(Shape s in ToDraw)
               {
                   sw.WriteLine(s.SaveText());
               }
               sw.Flush();
               sw.Close();
               filename.Text = Path.GetFileName(saveFileDialog1.FileName);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Intersects.Clear();
            inters.Clear();
            ToDraw.Clear();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                while (sr.Peek() != -1)
                {
                    string[] line = sr.ReadLine().Split(';');
                    if(line[0] == "Line")
                    {
                        ToDraw.Add(new Line(Convert.ToInt32(line[1]), Convert.ToInt32(line[2]), Convert.ToInt32(line[3]), Convert.ToInt32(line[4]), Color.FromArgb(Convert.ToInt32(line[5])), Convert.ToDecimal(line[6])));
                    }
                    else if(line[0] == "Rectangle")
                    {
                        ToDraw.Add(new Rectangle(Convert.ToInt32(line[1]), Convert.ToInt32(line[2]), Convert.ToInt32(line[3]), Convert.ToInt32(line[4]), true, Color.FromArgb(Convert.ToInt32(line[6])), Convert.ToDecimal(line[7])));
                    }
                    else if(line[0] == "Circle")
                    {
                        ToDraw.Add(new Circle(Convert.ToInt32(line[1]), Convert.ToInt32(line[2]), Convert.ToInt32(line[3]), Color.FromArgb(Convert.ToInt32(line[4])), Convert.ToDecimal(line[5])));
                    }
                }
                sr.Close();
                panel1.Invalidate();
                filename.Text = Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void button_inter_Click(object sender, EventArgs e){



            Line l1 = (Line)selectedItem;
            for (int i = 0; i < ToDraw.Count; i++)
            {
                if (ToDraw[i] is Line)
                {
                    Line l2 = (Line)ToDraw[i];
                    IntersectionsLine(l1, l2);
                }
            }
            panel1.Invalidate();
            

        }

        private void IntersectionsLine(Line l1, Line l2)
        {
            double valuex = 0;
            double valuey = 0;

            
            // if ((l1.k == l2.k) || (l1.d == l2.d))
            if (l1.k == l2.k)
                return;

            if ((l1.k == 0) && (l2.k == 123456))
            {
                valuex = l2.start.X;
                valuey = l1.start.Y;
            }
            else if ((l2.k == 0) && (l1.k == 123456))
            {
                valuex = l1.start.X;
                valuey = l2.start.Y;
            }

            //senkrecht
            else if (l2.k == 123456)
            {
                valuey = l1.k * l2.start.X + l1.d;
                valuex = (valuey - l1.d) / l1.k;
            }
            else if (l1.k == 123456)
            {
                valuey = l2.k * l1.start.X + l2.d;
                valuex = (valuey - l2.d) / l2.k;
            }

            //waagrecht
            else if (l1.k == 0)
            {
                valuex = (l1.start.Y - l2.d) / l2.k;
                valuey = l2.k * valuex + l2.d;
            }
            else if (l2.k == 0)
            {
                valuex = (l2.start.Y - l1.d) / l1.k;
                valuey = l1.k * valuex + l1.d;
            }

            else
            {
                valuex = (l2.d - l1.d) / (l1.k - l2.k);
                valuey = l1.k * valuex + l1.d;
            }
            int vx = Convert.ToInt32(valuex);
            int vy = Convert.ToInt32(valuey);
            Point coords = new Point(vx, vy);
            Rectangle area1 = (Rectangle)l1.ReturnArea();
            Rectangle area2 = (Rectangle)l2.ReturnArea();
            if (((isBetween(area1.top.X, area1.top.X + area1.width, coords.X)) && (isBetween(area1.top.Y, area1.top.Y + area1.height, coords.Y))) && ((isBetween(area2.top.X, area2.top.X + area2.width, coords.X)) && (isBetween(area2.top.Y, area2.top.Y + area2.height, coords.Y))))
            {

                Circle c = new Circle(vx, vy, 5, Color.Red, 3);
                Intersects.Add(c);
                inters.Add(new Font(vx + 10, vy + 10, "(" + vx + "|" + vy + ")"));
            }
            else
            {
                return;
            }
        }

        // all intersects
        private void button8_Click(object sender, EventArgs e)
        {

            Line l1 = (Line)selectedItem;
            for (int i = 0; i < ToDraw.Count; i++)
            {
                if (ToDraw[i] is Line)
                {
                    Line l2 = (Line)ToDraw[i];
                    IntersectionsLine(l1, l2);
                }
                if(ToDraw[i] is Rectangle)
                {
                    Rectangle r1 = (Rectangle)ToDraw[i];
                    List<Shape> check = new List<Shape>();
                    check.Add(new Line(r1.top.X, r1.top.Y, r1.top.X, r1.bottom.Y, Color.Red, 1));
                    check.Add(new Line(r1.top.X, r1.bottom.Y, r1.bottom.X, r1.bottom.Y, Color.Red, 1));
                    check.Add(new Line(r1.bottom.X, r1.top.Y, r1.bottom.X, r1.bottom.Y, Color.Red, 1));
                    check.Add(new Line(r1.bottom.X, r1.top.Y, r1.top.X, r1.top.Y, Color.Red, 1));
                    foreach (Line c in check)
                    {
                        IntersectionsLine(l1, c);
                    }
                }

                if(ToDraw[i] is Circle)
                {
                    Circle c = (Circle)ToDraw[i];
                    if (l1.k == 0)
                    {
                        l1.k = 0.0001;
                        l1.d = l1.start.Y;
                    }
                    double k = l1.k;
                    double k2 = Math.Pow(l1.k, 2);
                    double d2 = Math.Pow(l1.d, 2);
                    double d = l1.d;
                    if(l1.k == 123456)
                    {
                         k = (Convert.ToDouble(l1.end.Y) - Convert.ToDouble(l1.start.Y)) / (Convert.ToDouble(l1.end.X+1) - Convert.ToDouble(l1.start.X));
                         d = l1.end.Y-k*l1.end.X;
                        
                        k2 = Math.Pow(k, 2);
                       
                        d2 = Math.Pow(d, 2);
                    }
                    

                    
                    
                    double r2 = Math.Pow(c.rad,2);
                   
                    double mx2 = Math.Pow(c.middle.X,2);
                    double my2 = Math.Pow(c.middle.Y,2);
                    
                    double r = c.rad;
                   
                    double mx = c.middle.X;
                    double my = c.middle.Y;

                    double ergx = (Math.Sqrt((k2+1)*r2-d2-2*d*(k*mx-my)-k2*mx2+2*k*mx*my-my2)-d*k+k*my+mx)/(k2+1);
                    double ergy = k * ergx + d;

                    if(double.IsNaN(ergx)||double.IsNaN(ergy))
                    {
                        return;
                    }
                    int vx = Convert.ToInt32(ergx);
                    int vy = Convert.ToInt32(ergy);
                    Circle c1 = new Circle(vx, vy, 5, Color.Red, 3);
                    Intersects.Add(c1);
                    inters.Add(new Font(vx + 10, vy + 10, "(" + vx + "|" + vy + ")"));

                    ergx = -(Math.Sqrt((k2+1)*r2-d2-2*d*(k*mx-my)-k2*mx2+2*k*mx*my-my2)+d*k-k*my-mx)/(k2+1);
                    ergy = k * ergx + d;

                    if (double.IsNaN(ergx) || double.IsNaN(ergy))
                    {
                        return;
                    }
                    vx = Convert.ToInt32(ergx);
                    vy = Convert.ToInt32(ergy);
                    Circle c2 = new Circle(vx, vy, 5, Color.Red, 3);
                    Intersects.Add(c2);
                    inters.Add(new Font(vx + 10, vy + 10, "(" + vx + "|" + vy + ")"));
                }
            }
            panel1.Invalidate();
        }
        
        
        
    }
}
