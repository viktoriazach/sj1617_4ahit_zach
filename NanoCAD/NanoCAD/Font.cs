﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace NanoCAD
{
    class Font
    {
      

        public PointF p { get; set; }
        public string Text { get; set; }

        public Font(int x, int y, string txt)
        {
            p = new PointF(x, y);
            Text = txt;
        }

       
    }
}
