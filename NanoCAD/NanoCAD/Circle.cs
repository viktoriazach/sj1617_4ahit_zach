﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NanoCAD
{
    class Circle:Shape
    {

        public Point start { get; set; }
        public Point middle { get; set; }
        public int rad { get; set; }
      
        
        public Circle(int x1, int y1, int rad, Color c, decimal thick)
        {
            pen = new Pen(c, Convert.ToInt32(thick));
            this.rad = rad;
            middle = new Point(x1, y1);
            start = new Point(x1-rad, y1-rad);
        }

        public override void DrawMe(PaintEventArgs e)
        {
            //dosth
            e.Graphics.DrawEllipse(pen, start.X, start.Y, rad*2, rad*2);
        }

        public override void DrawMarkers(PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Red, 1f);
            e.Graphics.DrawRectangle(pen, start.X + rad -3, start.Y-3, 6, 6);
            e.Graphics.DrawRectangle(pen, start.X + rad - 3, start.Y + 2 * rad - 3, 6, 6);
            e.Graphics.DrawRectangle(pen, start.X - 3, start.Y + rad - 3, 6, 6);
            e.Graphics.DrawRectangle(pen,  start.X + 2 * rad - 3, start.Y + rad - 3, 6, 6);
        }

        public override Shape ReturnArea()
        {
            return new Rectangle(start.X, start.Y, rad*2, rad*2, Color.Red, 1);
        }

        public override string SaveText()
        {
            return String.Format("Circle;{0};{1};{2};{3};{4}", middle.X, middle.Y, rad, pen.Color.ToArgb(), pen.Width);
        }
    }
}
