﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NanoCAD
{
    
    public abstract class Shape
    {

        public Pen pen;

        public bool amIselected = false;

        public abstract void DrawMarkers(PaintEventArgs e);

        public abstract void DrawMe(PaintEventArgs e);

        public abstract Shape ReturnArea();

        public abstract string SaveText();
    }
}
