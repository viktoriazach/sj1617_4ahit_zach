﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace NanoCAD
{
    class Rectangle:Shape
    {

        public Point top { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public Point bottom;

        

        public Rectangle(int x1, int y1, int x2, int y2, bool b, Color c, decimal thick)
        {
            pen = new Pen(c, Convert.ToInt32(thick));
            if (y2 < y1)
            {
                int temp = y2;
                y2 = y1;
                y1 = temp;
            }

            if (x1 > x2)
            {
                int temp = x2;
                x2 = x1;
                x1 = temp;
            }
            
            top = new Point(x1, y1);
            bottom = new Point(x2, y2);

            width = x2 - x1;
            height = y2 - y1;
            
            
        }

        public Rectangle(int x1, int y1, int w, int h, Color c, decimal thick)
        {
            
            pen = new Pen(c, Convert.ToInt32(thick));

            this.width = w;
            this.height = h;
            top = new Point(x1, y1);

        }    

        public override void DrawMe(PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(pen, top.X, top.Y, width, height);
           
        }
        
        public override void DrawMarkers(PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Red, 1f);
            e.Graphics.DrawRectangle(pen, top.X - 3, top.Y - 3, 6, 6);
            e.Graphics.DrawRectangle(pen, top.X + width - 3, top.Y - 3, 6, 6);
            e.Graphics.DrawRectangle(pen, top.X - 3, top.Y + height- 3, 6, 6);
            e.Graphics.DrawRectangle(pen, top.X + width - 3, top.Y + height - 3, 6, 6);
        }

        public override Shape ReturnArea()
        {
            return new Rectangle(this.top.X, this.top.Y, this.width, this.height, Color.Red, 1);
        }

        public override string SaveText()
        {
            return String.Format("Rectangle;{0};{1};{2};{3};{4};{5};{6}", top.X, top.Y, bottom.X, bottom.Y, true, pen.Color.ToArgb(), pen.Width);
        }

    }
}
