﻿namespace NanoCAD
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.p_line = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.line_y2 = new System.Windows.Forms.NumericUpDown();
            this.line_x2 = new System.Windows.Forms.NumericUpDown();
            this.line_y1 = new System.Windows.Forms.NumericUpDown();
            this.line_x1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.p_circle = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.circle_rad = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.circle_y1 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.circle_x1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.p_rectangle = new System.Windows.Forms.Panel();
            this.rectangle_height = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.rectangle_width = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.rectangle_y1 = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.rectangle_x1 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.rectangle_x2 = new System.Windows.Forms.NumericUpDown();
            this.rectangle_y2 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.button7 = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.thick = new System.Windows.Forms.NumericUpDown();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filename = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button_inter = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.p_line.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.line_y2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.line_x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.line_y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.line_x1)).BeginInit();
            this.p_circle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.circle_rad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circle_y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circle_x1)).BeginInit();
            this.p_rectangle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_y1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_y2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(264, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = " Choose your shape:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 24;
            this.listBox1.Items.AddRange(new object[] {
            "Line",
            "Circle",
            "Rectangle"});
            this.listBox1.Location = new System.Drawing.Point(119, 144);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(213, 76);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // p_line
            // 
            this.p_line.BackColor = System.Drawing.Color.HotPink;
            this.p_line.Controls.Add(this.label8);
            this.p_line.Controls.Add(this.label7);
            this.p_line.Controls.Add(this.label6);
            this.p_line.Controls.Add(this.label5);
            this.p_line.Controls.Add(this.line_y2);
            this.p_line.Controls.Add(this.line_x2);
            this.p_line.Controls.Add(this.line_y1);
            this.p_line.Controls.Add(this.line_x1);
            this.p_line.Controls.Add(this.label2);
            this.p_line.Location = new System.Drawing.Point(35, 268);
            this.p_line.Name = "p_line";
            this.p_line.Size = new System.Drawing.Size(496, 241);
            this.p_line.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(258, 155);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 25);
            this.label8.TabIndex = 8;
            this.label8.Text = "y2:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(258, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 25);
            this.label7.TabIndex = 7;
            this.label7.Text = "x2:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 25);
            this.label6.TabIndex = 6;
            this.label6.Text = "y1:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 25);
            this.label5.TabIndex = 5;
            this.label5.Text = "x1:";
            // 
            // line_y2
            // 
            this.line_y2.Location = new System.Drawing.Point(303, 153);
            this.line_y2.Name = "line_y2";
            this.line_y2.Size = new System.Drawing.Size(89, 29);
            this.line_y2.TabIndex = 4;
            // 
            // line_x2
            // 
            this.line_x2.Location = new System.Drawing.Point(303, 81);
            this.line_x2.Name = "line_x2";
            this.line_x2.Size = new System.Drawing.Size(89, 29);
            this.line_x2.TabIndex = 3;
            this.line_x2.ValueChanged += new System.EventHandler(this.line_x2_ValueChanged);
            // 
            // line_y1
            // 
            this.line_y1.Location = new System.Drawing.Point(77, 153);
            this.line_y1.Name = "line_y1";
            this.line_y1.Size = new System.Drawing.Size(89, 29);
            this.line_y1.TabIndex = 2;
            // 
            // line_x1
            // 
            this.line_x1.Location = new System.Drawing.Point(77, 81);
            this.line_x1.Name = "line_x1";
            this.line_x1.Size = new System.Drawing.Size(89, 29);
            this.line_x1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = "Line";
            // 
            // p_circle
            // 
            this.p_circle.BackColor = System.Drawing.Color.HotPink;
            this.p_circle.Controls.Add(this.label11);
            this.p_circle.Controls.Add(this.circle_rad);
            this.p_circle.Controls.Add(this.label9);
            this.p_circle.Controls.Add(this.circle_y1);
            this.p_circle.Controls.Add(this.label10);
            this.p_circle.Controls.Add(this.circle_x1);
            this.p_circle.Controls.Add(this.label3);
            this.p_circle.Location = new System.Drawing.Point(285, 93);
            this.p_circle.Name = "p_circle";
            this.p_circle.Size = new System.Drawing.Size(496, 241);
            this.p_circle.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(248, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 25);
            this.label11.TabIndex = 12;
            this.label11.Text = "Rad:";
            // 
            // circle_rad
            // 
            this.circle_rad.Location = new System.Drawing.Point(303, 84);
            this.circle_rad.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.circle_rad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.circle_rad.Name = "circle_rad";
            this.circle_rad.Size = new System.Drawing.Size(89, 29);
            this.circle_rad.TabIndex = 11;
            this.circle_rad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "y1:";
            // 
            // circle_y1
            // 
            this.circle_y1.Location = new System.Drawing.Point(77, 154);
            this.circle_y1.Name = "circle_y1";
            this.circle_y1.Size = new System.Drawing.Size(89, 29);
            this.circle_y1.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 25);
            this.label10.TabIndex = 9;
            this.label10.Text = "x1:";
            // 
            // circle_x1
            // 
            this.circle_x1.Location = new System.Drawing.Point(77, 82);
            this.circle_x1.Name = "circle_x1";
            this.circle_x1.Size = new System.Drawing.Size(89, 29);
            this.circle_x1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "Circle";
            // 
            // p_rectangle
            // 
            this.p_rectangle.BackColor = System.Drawing.Color.HotPink;
            this.p_rectangle.Controls.Add(this.rectangle_height);
            this.p_rectangle.Controls.Add(this.label17);
            this.p_rectangle.Controls.Add(this.rectangle_width);
            this.p_rectangle.Controls.Add(this.label16);
            this.p_rectangle.Controls.Add(this.label12);
            this.p_rectangle.Controls.Add(this.label4);
            this.p_rectangle.Controls.Add(this.label13);
            this.p_rectangle.Controls.Add(this.rectangle_y1);
            this.p_rectangle.Controls.Add(this.label14);
            this.p_rectangle.Controls.Add(this.rectangle_x1);
            this.p_rectangle.Controls.Add(this.label15);
            this.p_rectangle.Controls.Add(this.rectangle_x2);
            this.p_rectangle.Controls.Add(this.rectangle_y2);
            this.p_rectangle.Location = new System.Drawing.Point(266, 15);
            this.p_rectangle.Name = "p_rectangle";
            this.p_rectangle.Size = new System.Drawing.Size(496, 287);
            this.p_rectangle.TabIndex = 4;
            // 
            // rectangle_height
            // 
            this.rectangle_height.Location = new System.Drawing.Point(303, 213);
            this.rectangle_height.Name = "rectangle_height";
            this.rectangle_height.Size = new System.Drawing.Size(89, 29);
            this.rectangle_height.TabIndex = 19;
            this.rectangle_height.ValueChanged += new System.EventHandler(this.rectangle_height_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(228, 215);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 25);
            this.label17.TabIndex = 20;
            this.label17.Text = "Height:";
            // 
            // rectangle_width
            // 
            this.rectangle_width.Location = new System.Drawing.Point(78, 215);
            this.rectangle_width.Name = "rectangle_width";
            this.rectangle_width.Size = new System.Drawing.Size(89, 29);
            this.rectangle_width.TabIndex = 17;
            this.rectangle_width.ValueChanged += new System.EventHandler(this.rectangle_width_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 217);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 25);
            this.label16.TabIndex = 18;
            this.label16.Text = "Width:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(258, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 25);
            this.label12.TabIndex = 16;
            this.label12.Text = "y2:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "Rectangle";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(258, 78);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 25);
            this.label13.TabIndex = 15;
            this.label13.Text = "x2:";
            // 
            // rectangle_y1
            // 
            this.rectangle_y1.Location = new System.Drawing.Point(77, 148);
            this.rectangle_y1.Name = "rectangle_y1";
            this.rectangle_y1.Size = new System.Drawing.Size(89, 29);
            this.rectangle_y1.TabIndex = 10;
            this.rectangle_y1.ValueChanged += new System.EventHandler(this.rectangle_y1_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(32, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 25);
            this.label14.TabIndex = 14;
            this.label14.Text = "y1:";
            // 
            // rectangle_x1
            // 
            this.rectangle_x1.Location = new System.Drawing.Point(77, 76);
            this.rectangle_x1.Name = "rectangle_x1";
            this.rectangle_x1.Size = new System.Drawing.Size(89, 29);
            this.rectangle_x1.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(32, 78);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 25);
            this.label15.TabIndex = 13;
            this.label15.Text = "x1:";
            // 
            // rectangle_x2
            // 
            this.rectangle_x2.Location = new System.Drawing.Point(303, 76);
            this.rectangle_x2.Name = "rectangle_x2";
            this.rectangle_x2.Size = new System.Drawing.Size(89, 29);
            this.rectangle_x2.TabIndex = 11;
            this.rectangle_x2.ValueChanged += new System.EventHandler(this.rectangle_x2_ValueChanged);
            // 
            // rectangle_y2
            // 
            this.rectangle_y2.Location = new System.Drawing.Point(303, 148);
            this.rectangle_y2.Name = "rectangle_y2";
            this.rectangle_y2.Size = new System.Drawing.Size(89, 29);
            this.rectangle_y2.TabIndex = 12;
            this.rectangle_y2.ValueChanged += new System.EventHandler(this.rectangle_y2_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 1039);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(543, 91);
            this.button1.TabIndex = 5;
            this.button1.Text = "Draw";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Pink;
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(583, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1136, 1089);
            this.panel1.TabIndex = 0;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 955);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(267, 78);
            this.button2.TabIndex = 6;
            this.button2.Text = "Clear all";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(285, 955);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(270, 78);
            this.button3.TabIndex = 7;
            this.button3.Text = "Redo";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(112, 885);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 50);
            this.button4.TabIndex = 8;
            this.button4.Text = "+";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 898);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 25);
            this.label18.TabIndex = 9;
            this.label18.Text = "Zoom:";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(190, 885);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 50);
            this.button5.TabIndex = 10;
            this.button5.Text = "-";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(285, 871);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(270, 78);
            this.button6.TabIndex = 11;
            this.button6.Text = "Delete selected";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(298, 972);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 47);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(17, 972);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(61, 47);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(169, 1060);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(61, 47);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 14;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(298, 885);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(48, 47);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 15;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.LavenderBlush;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(43, 144);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(63, 73);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 16;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(22, 797);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 47);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 18;
            this.pictureBox6.TabStop = false;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(9, 782);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(270, 78);
            this.button7.TabIndex = 17;
            this.button7.Text = "Change color";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.LavenderBlush;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(294, 782);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(52, 78);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 19;
            this.pictureBox7.TabStop = false;
            // 
            // thick
            // 
            this.thick.Location = new System.Drawing.Point(380, 807);
            this.thick.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.thick.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.thick.Name = "thick";
            this.thick.Size = new System.Drawing.Size(89, 29);
            this.thick.TabIndex = 21;
            this.thick.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.LavenderBlush;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(503, 782);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(52, 78);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 22;
            this.pictureBox9.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1731, 38);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(56, 34);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(150, 34);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(150, 34);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(99, 34);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // filename
            // 
            this.filename.AutoSize = true;
            this.filename.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.857143F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filename.Location = new System.Drawing.Point(12, 5);
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(171, 29);
            this.filename.TabIndex = 0;
            this.filename.Text = "unnamed.ncad";
            this.filename.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.filename.Click += new System.EventHandler(this.filename_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DeepPink;
            this.panel2.Controls.Add(this.filename);
            this.panel2.Location = new System.Drawing.Point(583, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1136, 38);
            this.panel2.TabIndex = 1;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "CAD|*.ncad";
            // 
            // button_inter
            // 
            this.button_inter.Location = new System.Drawing.Point(9, 695);
            this.button_inter.Name = "button_inter";
            this.button_inter.Size = new System.Drawing.Size(270, 81);
            this.button_inter.TabIndex = 1;
            this.button_inter.Text = "Mark line-intersections";
            this.button_inter.UseVisualStyleBackColor = true;
            this.button_inter.Click += new System.EventHandler(this.button_inter_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(285, 695);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(270, 81);
            this.button8.TabIndex = 24;
            this.button8.Text = "Mark ALL intersections";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LavenderBlush;
            this.ClientSize = new System.Drawing.Size(1731, 1142);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button_inter);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.thick);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.p_rectangle);
            this.Controls.Add(this.p_circle);
            this.Controls.Add(this.p_line);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "       NanoCAD";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.p_line.ResumeLayout(false);
            this.p_line.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.line_y2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.line_x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.line_y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.line_x1)).EndInit();
            this.p_circle.ResumeLayout(false);
            this.p_circle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.circle_rad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circle_y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circle_x1)).EndInit();
            this.p_rectangle.ResumeLayout(false);
            this.p_rectangle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_y1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectangle_y2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel p_line;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel p_circle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel p_rectangle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown line_y2;
        private System.Windows.Forms.NumericUpDown line_x2;
        private System.Windows.Forms.NumericUpDown line_y1;
        private System.Windows.Forms.NumericUpDown line_x1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown circle_rad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown circle_y1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown circle_x1;
        private System.Windows.Forms.NumericUpDown rectangle_height;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown rectangle_width;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown rectangle_y1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown rectangle_x1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown rectangle_x2;
        private System.Windows.Forms.NumericUpDown rectangle_y2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.NumericUpDown thick;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label filename;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button_inter;
        private System.Windows.Forms.Button button8;
    }
}

