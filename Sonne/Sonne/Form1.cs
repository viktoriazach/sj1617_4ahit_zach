﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sonne
{
    public partial class Form1 : Form
    {
        int angle = 10;
        public Form1()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(this.ClientRectangle.Width / 2, this.ClientRectangle.Height / 2);
            e.Graphics.FillEllipse(Brushes.Yellow, -100, -100, 200, 200);
            e.Graphics.RotateTransform(angle);
            e.Graphics.TranslateTransform(200, 0);
            //draw the earth

            e.Graphics.FillEllipse(Brushes.Blue, -5, -5, 40, 40);
           
            e.Graphics.RotateTransform(angle);

            e.Graphics.TranslateTransform(40, 0);
            e.Graphics.FillEllipse(Brushes.LightGray, -5, -5, 10, 10);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Invalidate();
            angle += 10;
        }
    }
}
