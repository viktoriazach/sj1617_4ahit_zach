﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;

namespace XPath
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            XMLOne(doc);
            XMLTwo(doc);
            XMLThree(doc);
            XMLFour(doc);
            XMLFive();
            Console.ReadLine();

            XmlDocument doc2 = new XmlDocument();
            doc2.Load("Test_777.tea");
            XMLSix(doc2);
            XMLSeven(doc2);
            XMLEight();
        }

        static void XMLOne(XmlDocument doc)
        {
            //XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte");
            XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte[. > 30]");
            Console.WriteLine("Aufgabe 1:");
            Console.WriteLine("-----------------------------------");
            foreach (XmlNode xn in res)
            {
                //Console.WriteLine(xn.InnerText);
            }
            Console.WriteLine("Anzahl mit mehr als 30 Punkten: "+ res.Count);
            Console.WriteLine();
        }

        static void XMLTwo(XmlDocument doc)
        {
            //XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte");
            XmlNodeList res = doc.SelectNodes("TestResult/Pupil[@ID=941]/Vorname");
            XmlNodeList res2 = doc.SelectNodes("TestResult/Pupil[@ID=941]/Nachname");
            Console.WriteLine("Aufgabe 2:");
            Console.WriteLine("-----------------------------------");
            foreach (XmlNode xn in res)
            {
                Console.Write(xn.InnerText);
            }
            Console.Write(" ");
            foreach (XmlNode xn in res2)
            {
                Console.WriteLine(xn.InnerText);
            }
            Console.WriteLine();
            //degree[@from != "Harvard"]
        }

        static void XMLThree(XmlDocument doc)
        {
            //XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte");
            XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte");
            Console.WriteLine("Aufgabe 3:");
            Console.WriteLine("-----------------------------------");
            double sum = 0;
            foreach (XmlNode xn in res)
            {
                double s =0;
                if(double.TryParse(xn.InnerText, out s))
                { sum += s;
              
                }
              
            }
            sum /= 10;
            XmlNode res2 = doc.SelectSingleNode("TestResult/@SumPunkte");
            string sum2 = res2.InnerText;
            Console.WriteLine("Berechnete Summe: "+sum);
            Console.WriteLine("Vorgegebene Summe: "+sum2);
            Console.WriteLine();
        }

        static void XMLFour(XmlDocument doc)
        {
            //XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte");
            XmlNodeList res = doc.SelectNodes("TestResult/Pupil/Punkte");
            Console.WriteLine("Aufgabe 4:");
            Console.WriteLine("-----------------------------------");
            double sum = 0;
            double anz = 0;
            foreach (XmlNode xn in res)
            {
                double s = 0;
                if (double.TryParse(xn.InnerText, out s))
                {
                    sum += s;
                    anz++;
                }

            }
            sum /= 10;
            sum = sum / anz;
           sum = Math.Round(sum, 2);
            Console.WriteLine("Mittelwert: " + sum);
            Console.WriteLine();
        }

        static void XMLFive()
        {
            Data d = new Data();
            d.ShowDialog();
        }

        static void XMLSix(XmlDocument doc)
        {
            //XmlNodeList res = doc.SelectNodes("Test/EntryList/Exercise/Points[.>2]");
            XmlNodeList res = doc.SelectNodes("//Points[.>2]");
            Console.WriteLine("Aufgabe 6:");
            Console.WriteLine("-----------------------------------");
            foreach (XmlNode xn in res)
            {
               // Console.WriteLine(xn.InnerText);
            }
            Console.WriteLine("Fragen mit mehr als 2 Punkten: "+res.Count);
            Console.WriteLine();
        }

        static void XMLSeven(XmlDocument doc)
        {
            XmlNodeList res = doc.SelectNodes("*/*/Exercise[@ID = 16]/subExerciseNode/*/Value");
            Console.WriteLine("Aufgabe 7:");
            Console.WriteLine("-----------------------------------");
            foreach (XmlNode xn in res)
            {
                // Console.WriteLine(xn.InnerText);
            }
            Console.WriteLine("Fragen mit mehr als 2 Punkten: " + res.Count);
            Console.WriteLine();
        }

        static void XMLEight()
        {
            Console.WriteLine("Aufgabe 8:");
            Console.WriteLine("-----------------------------------");
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            XmlDocument doc2 = new XmlDocument();
            doc2.Load("Test_777.tea");

            XmlNodeList res2 = doc2.SelectNodes("Test/Infos/Grading/Schulnoten/Grenze");
            Dictionary<int,int> grades = new Dictionary<int,int>();
            foreach (XmlNode xn in res2)
	{
        string s1 = xn.SelectSingleNode("@Note").InnerText;
        string s2 = xn.SelectSingleNode("@Prz").InnerText;

        grades.Add(Convert.ToInt32(s1), Convert.ToInt32(s2));
	}
            grades.Add(0, 101);
            XmlNodeList res = doc.SelectNodes("TestResult/Pupil[./Prozent != '-']");
            foreach (XmlNode xn in res)
            {
                double pro = Convert.ToDouble(xn.SelectSingleNode("Prozent").InnerText.Replace('%',' '))/10;
                string note = xn.SelectSingleNode("Note").InnerText;
                int not;
                
                if (note.Length == 4)
                     not = Convert.ToInt32(note[1]+"");
                else
                    not = Convert.ToInt32(note[0]+"");
                if((grades[not] < pro)&&(grades[not-1] > pro))
                {
                   // Console.WriteLine("true");
                }
                else
                    Console.WriteLine(xn.SelectSingleNode("Nachname").InnerText);
                
            }
                
            
        }

    }
}
