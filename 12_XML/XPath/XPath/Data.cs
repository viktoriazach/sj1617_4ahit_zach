﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace XPath
{
    public partial class Data : Form
    {
        public Data()
        {
          
            InitializeComponent();
            XmlReader xmlFile;
            xmlFile = XmlReader.Create("Result.tea", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            dataGridView1.DataSource = ds.Tables[1];
        }
    }
}
