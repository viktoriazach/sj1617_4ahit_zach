﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patienten
{
    class Pat_Treat
    {
        public int id;
        public int dauer;
        public int beh;
        public string date;
        public Pat_Treat(int i, int b, int d, string da)
        {
            date = da;
            id = i;
            beh = b;
            dauer = d;
        }
    }
}
