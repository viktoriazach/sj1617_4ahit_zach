﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
namespace Uebung1
{
    class Program
    {
        static void Main(string[] args)
        {

            string s = "<root><schueler>Tanja</schueler></root>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(s);

            XmlNode xn = doc.CreateElement("schueler");
            xn.InnerText = "Sonja";

            XmlAttribute a = doc.CreateAttribute("id");
            a.InnerText = "3";
            xn.Attributes.Append(a);

            XmlNode root = doc.SelectSingleNode("root");
            root.AppendChild(xn);

            //doc.AppendChild(xn);

            doc.Save("dummy.xml");
            //doc.Load(filename);

        }
    }
}
