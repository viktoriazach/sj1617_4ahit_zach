﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Floorplan
{
    abstract public class Symbol
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Symbol(int x, int y)
        {
            X = x;
            Y = y;
        }

        public abstract void DrawMe(PaintEventArgs e);
        public abstract string SaveMe();

    }
}
