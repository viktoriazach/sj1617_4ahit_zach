﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Floorplan
{
   public class FireExtinguisher:Symbol
    {
        public FireExtinguisher(int x, int y):base(x,y)
        {

        }

        public override void DrawMe(System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Color.Crimson), X, Y, 20, 40);
            e.Graphics.DrawRectangle(new Pen(Color.Black,2), X, Y, 20, 40);
            e.Graphics.DrawLine(new Pen(Color.Black,2), new Point(X, Y), new Point(X + 20, Y + 40));
        }

        public override string SaveMe()
        {
            return "FireExtinguisher;" + X + ";" + Y;
        }

        
    }
}
