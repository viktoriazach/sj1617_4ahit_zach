﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Floorplan
{
    class Room:Symbol
    {
        string name;
        public Room(int x, int y, string room):base(x,y)
        {
            name = room;
        }

        public override void DrawMe(System.Windows.Forms.PaintEventArgs e)
        {
            //int length = name.Length * 7;

            SizeF leng = e.Graphics.MeasureString(name, new Font(new FontFamily("Arial"), 8));
             e.Graphics.FillRectangle(new SolidBrush(Color.Turquoise), new Rectangle(X,Y, Convert.ToInt32(leng.Width), Convert.ToInt32(leng.Height)));
            e.Graphics.DrawString(name, new Font(new FontFamily("Arial"), 8), new SolidBrush(Color.Black), new Point(X, Y));
          
        }

        public override string SaveMe()
        {
            return "Room;" + X + ";" + Y + ";" + name;
        }
    }
}
