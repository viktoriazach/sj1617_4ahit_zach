﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Floorplan
{
    public partial class Form1 : Form
    {
        Point lastRightMouse;
        public string actualpicture;
        public List<Symbol> symbs = new List<Symbol>();
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Height = 400;
            pictureBox1.Width = 600;
            lastRightMouse = new Point(0, 0);
            pictureBox1.Enabled = false;
        }


        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void fireExtinguisherToolStripMenuItem_Click(object sender, EventArgs e)
        {

            symbs.Add(new FireExtinguisher(lastRightMouse.X, lastRightMouse.Y));
            pictureBox1.Invalidate();
           
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Enabled = true;
            symbs.Clear();
            openFileDialog1.Title = "Choose the floorplan...";
            openFileDialog1.Filter = "JPEG Files (*.jpeg)|*.jpg";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                actualpicture = openFileDialog1.FileName;
            }
            //MessageBox.Show(actualpicture);
            pictureBox1.BackgroundImage = Image.FromFile(actualpicture);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Symbol s in symbs)
            {
                s.DrawMe(e);
            }
        }

        private void radioactivToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            symbs.Add(new Radioactive(lastRightMouse.X, lastRightMouse.Y));
            pictureBox1.Invalidate();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            symbs.Add(new Exit(lastRightMouse.X, lastRightMouse.Y));
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
            Point coords = pictureBox1.PointToClient(Cursor.Position);
            for (int i = 0; i < symbs.Count; i++)
            {
                if((IsBetween(coords.X, symbs[i].X, symbs[i].X+15)) && (IsBetween(coords.Y, symbs[i].Y, symbs[i].Y+20)))
                {
                    symbs.Remove(symbs[i]);
                }

            }
            pictureBox1.Invalidate();
            
        }

        public bool IsBetween(int check, int v1, int v2)
        {
            if ((check >= v1) && (check <= v2))
                return true;

            return false;
        }

        private void roomIdentifyerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            roomid rid = new roomid();
            rid.StartPosition = FormStartPosition.CenterParent;
            if(rid.ShowDialog() == DialogResult.OK){
                symbs.Add(new Room(lastRightMouse.X, lastRightMouse.Y, rid.textBox1.Text));
            }
           
            pictureBox1.Invalidate();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = ".ncad";
            // savedialog sv = new savedialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);
                sw.WriteLine(actualpicture);
                foreach (Symbol s in symbs)
                {
                    sw.WriteLine(s.SaveMe());
                }
                sw.Flush();
                sw.Close();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Enabled = true;
            openFileDialog1.Filter = "PLAN Files (*.plan)|*.plan";
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                symbs.Clear();
                actualpicture = sr.ReadLine();
                pictureBox1.BackgroundImage = Image.FromFile(actualpicture);
                while(sr.Peek() != -1)
                {

                    string zeile = sr.ReadLine();
                    string[] z = zeile.Split(';');
                    if (z[0] == "Room")
                        symbs.Add(new Room(Convert.ToInt32(z[1]), Convert.ToInt32(z[2]), z[3]));
                    if (z[0] == "Exit")
                        symbs.Add(new Exit(Convert.ToInt32(z[1]), Convert.ToInt32(z[2])));
                    if (z[0] == "Radioactive")
                        symbs.Add(new Radioactive(Convert.ToInt32(z[1]), Convert.ToInt32(z[2])));
                    if (z[0] == "FireExtinguisher")
                        symbs.Add(new FireExtinguisher(Convert.ToInt32(z[1]), Convert.ToInt32(z[2])));


                }
            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
          
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(Cursor.Position);
                lastRightMouse = new Point(e.X, e.Y);
            }
        }
    }
}
