﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Floorplan
{
    class Exit:Symbol
    {
        public Exit(int x, int y):base(x,y)
        {

        }

        public override void DrawMe(System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(Color.GreenYellow), X, Y, 40, 20);
           
        }

        public override string SaveMe()
        {
            return "Exit;" + X + ";" + Y;
        }
    }
}
