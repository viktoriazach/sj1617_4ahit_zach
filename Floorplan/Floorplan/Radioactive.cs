﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Floorplan
{
    class Radioactive:Symbol
    {

        public Radioactive(int x, int y):base(x,y)
        {
        
        }

        public override void DrawMe(System.Windows.Forms.PaintEventArgs e)
        {
            Point[] points = new Point[4];
            points[0] = new Point(4+X, 7+Y);
            points[1] = new Point(7+X, 9+Y);
            points[2] = new Point(4+X, 11+Y);
            points[3] = new Point(2+X, 9+Y);

            Point[] points1 = new Point[4];
            points1[0] = new Point(9 + X, 9 + Y);
            points1[1] = new Point(11 + X, 7 + Y);
            points1[2] = new Point(13 + X, 9 + Y);
            points1[3] = new Point(11 + X, 11 + Y);

            e.Graphics.FillPie(new SolidBrush(Color.Yellow), new Rectangle(X, Y, 15, 15), 360, 360);
            e.Graphics.DrawEllipse(new Pen(Color.Black), new Rectangle(X, Y, 15, 15));
            e.Graphics.FillPie(new SolidBrush(Color.Black), new Rectangle(X+6, Y+6, 3, 3), 360, 360);
            e.Graphics.FillRectangle(new SolidBrush(Color.Black), new Rectangle(X + 6, Y + 2, 4, 4));
            e.Graphics.FillPolygon(new SolidBrush(Color.Black), points);
            e.Graphics.FillPolygon(new SolidBrush(Color.Black), points1);
        }
        public override string SaveMe()
        {
            return "Radioactive;" + X + ";" + Y;
        }
    }
}
