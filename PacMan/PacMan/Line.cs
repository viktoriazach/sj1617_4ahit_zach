﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PacMan
{
    class Line:Shape
    {
        
        public Point start { get; set; }
        public Point end { get; set; }
   

        public double k { get; set; }
        public double d { get; set; }

        public Line(int x1, int y1, int x2, int y2, Color c, decimal thick)
        {
           
            pen = new Pen(c, Convert.ToInt32(thick));
            if (x1 <= x2)
            {
                start = new Point(x1, y1);
                end = new Point(x2, y2);
            }
            else
            {
                end = new Point(x1, y1);
                start = new Point(x2, y2);
            }

            if (x1 == x2)
            {
                k = 123456;
            }
            else if(y1 == y2)
            {
                k = 0;
            }
            else
            {
                k = (Convert.ToDouble(end.Y) - Convert.ToDouble(start.Y)) / (Convert.ToDouble(end.X) - Convert.ToDouble(start.X));
                d = end.Y-k*end.X;
            }
            
        }

        public override void DrawMe(PaintEventArgs e)
        {
            e.Graphics.DrawLine(pen, start, end);
        }

      ////  public override void DrawMarkers(PaintEventArgs e)
      //  {
      //      Pen pen = new Pen(Color.Red, 1f);
      //      e.Graphics.DrawRectangle(pen, start.X-3, start.Y-3, 6,6);
      //      e.Graphics.DrawRectangle(pen, end.X - 3, end.Y - 3, 6, 6);
            
      //  }

        ////public override Shape ReturnArea()
        //{
        //      if (start.Y == end.Y)
        //            return new Rectangle(start.X, start.Y - 10, end.X - start.X, 20, Color.Red, 1);

        //      if (end.Y - start.Y > 0)
        //      {
        //          if (start.X == end.X)
        //              return new Rectangle(start.X - 10, start.Y, 20, end.Y - start.Y, Color.Red, 1);
        //          return new Rectangle(start.X, start.Y, end.X - start.X, Math.Abs(end.Y - start.Y), Color.Red, 1);
        //      }

        //      int h = start.Y - end.Y;

        //      if (start.X == end.X)
        //          return new Rectangle(start.X - 10, end.Y, 20, end.Y - start.Y, Color.Red, 1);
        //      return new Rectangle(start.X, end.Y, end.X - start.X, Math.Abs(end.Y - start.Y), Color.Red, 1);
        //}

        //public override string SaveText()
        //{
        //    return String.Format("Line;{0};{1};{2};{3};{4};{5}", start.X, start.Y, end.X, end.Y, pen.Color.ToArgb(), pen.Width);
        //}
    }
}
