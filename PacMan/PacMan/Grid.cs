﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
namespace PacMan
{
    class Grid
    {
     public int gridSize;
     public int width;
       public int height;
        public List<Pill> pills;
        bool first = true;
        Random rand;
        public Grid(int width, int height, int gridSize)
        {
            this.gridSize = gridSize;
            this.width = width;
            this.height = height;
            pills = new List<Pill>();
            rand = new Random();
        }

        public void DrawRect(PaintEventArgs e, PictureBox panel1)
        {
            panel1.Height = height;
            panel1.Width = width;
            for (int z = gridSize; z <= panel1.Height - gridSize; z = z + (2 * gridSize))
            {
                for (int i = gridSize; i <= panel1.Width - gridSize; i = i + (2 * gridSize))
                {
                    //e.Graphics.DrawRectangle(new Pen(Brushes.White), i, z, gridSize, gridSize);
                    e.Graphics.FillRectangle(Brushes.Gray, i, z, gridSize, gridSize);
                }
            }


        }
        public void AddPills(PictureBox p)
        {
            for (int z = gridSize/2; z <= p.Height; z = z + (2 * gridSize))
            {
                for (int i = gridSize/2; i <= p.Width; i = i + (2 * gridSize))
                {
                    //e.Graphics.DrawRectangle(new Pen(Brushes.White), i, z, gridSize, gridSize);
                    if (rand.Next(0, 100) < 80)
                    { pills.Add(new Pill(gridSize/5, Color.Plum, i, z)); }
                    else
                        pills.Add(new SpecialPill(gridSize / 5, Color.FromArgb(255, 20, 148), i, z));
                }
            }
        }

        public void DrawPills(PaintEventArgs e, PictureBox p)
        {
            if(first)
            {
                AddPills(p);
                first = false;
            }

            foreach (Pill pi in pills)
            {
                pi.DrawMe(e); 
            }
            
        }

        public int CheckPills(Point point, Timer t)
        {
            if(pills.Count == 0)
            {
                t.Stop();
                MessageBox.Show("Gewonnen!");

                first = true;
                t.Start();
            }
            Pill chosen = new Pill(0,Color.Beige,0,0);
            for (int i = 0; i < pills.Count; i++)
            {
                if ((IsBetween(point.X, pills[i].pos.X - (gridSize / 2), pills[i].pos.X - (gridSize / 2)) && (IsBetween(point.Y, pills[i].pos.Y - (gridSize / 2), pills[i].pos.Y - (gridSize / 2)))))
                {
                    chosen = pills[i];
               
                }
                
            }
            pills.Remove(chosen);
            return chosen.value;
        }

        public bool IsBetween(int check, int v1, int v2)
        {
            if ((check >= v1) && (check <= v2))
                return true;

            return false;
        }

    }
}
