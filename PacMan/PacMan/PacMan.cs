﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace PacMan
{
    class PacMan
    {
        Color color;
        int width;
        int startX;
        int startY;
        int stepWidth;

        public int status = 1;
       
        public int xPos;
        public int yPos;

        public enum Alignment { north, south, east, west }
        public Alignment a;
       
        public Alignment newA;

        int gradStart = 40;
        int gradLenght = 280;
        int startmin = 0;
        int startmax = 40;
        bool close = true;
        
        public PacMan(Color color, int width, int startPositionX, int startPositionY, int stepWidth, Alignment a)
        {
            this.color = color;
            this.width = width;
            this.startX = startPositionX;
            this.startY = startPositionY;
            this.stepWidth = stepWidth;
            this.a = a;
            newA = a;
            xPos = startX;
            yPos = startY;
        }

        public void drawMe(PaintEventArgs e)
        {

            e.Graphics.FillPie(new SolidBrush(color), (int)xPos, (int)yPos, (int)width, (int)width, gradStart, gradLenght);
        }

        public int Move(Grid g, Timer t)
        {
            int value = 0;
            value = g.CheckPills(new Point(xPos,yPos),t);
            if(a == Alignment.south && newA == Alignment.north)
            {
                turnNorth(a);
                a = newA;
            }
            if (a == Alignment.north && newA == Alignment.south)
            {
                turnSouth(a);
                a = newA;
            }
            if (a == Alignment.east && newA == Alignment.west)
            {
                turnWest(a);
                a = newA;
            } if (a == Alignment.west && newA == Alignment.east)
            {
                turnEast(a);
                a = newA;
            }

            if((xPos%(g.gridSize*2) == 0)&&(yPos%(g.gridSize*2)==0))
            {
                if(a != newA)
                {
                    if (newA == Alignment.east)
                    {
                        turnEast(a);
                    }
                    if (newA == Alignment.north)
                    {
                        turnNorth(a);
                    }
                    if (newA == Alignment.south)
                    {
                        turnSouth(a);
                    }
                    if (newA == Alignment.west)
                    {
                        turnWest(a);
                    }
                    //continue
                }
                a= newA;
            }
            if (a == Alignment.east)
            {
                if (xPos == g.width)
                    xPos = -50;
                xPos += stepWidth;

                //turnEast(oldA);
            }
            if (a== Alignment.north)
            {
                if (yPos == 0)
                    yPos = g.height + 10;
                yPos -= stepWidth;
                //turnNorth(oldA);
            }
            if (a == Alignment.south)
            {
                if (yPos == g.height)
                    yPos = -50;
                yPos += stepWidth;
                // turnSouth(oldA);
            }
            if (a == Alignment.west)
            {
                if (xPos == 0)
                    xPos = g.width + 10;
                xPos -= stepWidth;
                // turnWest(oldA);
            }
            return value;
        }


        public void OpenClose()
        {
            if (gradStart == startmin)
                close = false;

            else if (gradStart == startmax)
                close = true;

            if (close)
            {
                gradStart -= 10;
                gradLenght += 20;
            }
            else
            {
                gradStart += 10;
                gradLenght -= 20;
            }
        }

        public void turnNorth(Alignment alignmentOld)
        {
            if ((alignmentOld == Alignment.east))
            {
                gradStart += 270;
                startmax += 270;
                startmin += 270;
            }
            else if (alignmentOld == Alignment.south)
            {
                gradStart += 180;
                startmax += 180;
                startmin += 180;
            }
            else if (alignmentOld == Alignment.west)
            {
                gradStart += 90;
                startmax += 90;
                startmin += 90;
            }
        }

        public void turnSouth(Alignment alignmentOld)
        {
            if (alignmentOld == Alignment.west)
            {
                gradStart += 270;
                startmax += 270;
                startmin += 270;
            }
            else if (alignmentOld == Alignment.north)
            {
                gradStart += 180;
                startmax += 180;
                startmin += 180;
            }
            else if (alignmentOld == Alignment.east)
            {
                gradStart += 90;
                startmax += 90;
                startmin += 90;
            }
        }

        public void turnWest(Alignment alignmentOld)
        {
            if (alignmentOld == Alignment.north)
            {
                gradStart += 270;
                startmax += 270;
                startmin += 270;
            }
            else if (alignmentOld == Alignment.east)
            {
                gradStart += 180;
                startmax += 180;
                startmin += 180;
            }
            else if (alignmentOld == Alignment.south)
            {
                gradStart += 90;
                startmax += 90;
                startmin += 90;
            }
        }

        public void turnEast(Alignment alignmentOld)
        {
            if (alignmentOld == Alignment.south)
            {
                gradStart += 270;
                startmax += 270;
                startmin += 270;
            }
            else if (alignmentOld == Alignment.west)
            {
                gradStart += 180;
                startmax += 180;
                startmin += 180;
            }
            else if (alignmentOld == Alignment.north)
            {
                gradStart += 90;
                startmax += 90;
                startmin += 90;
            }
        }

    }
}
