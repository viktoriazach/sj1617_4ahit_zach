﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacMan
{
    public partial class Form1 : Form
    {
        List<Shape> lines = new List<Shape>();
        PacMan p;
        Ghost ghost;
        int gridSize = 50;
        Grid g;
        int score = 0;
        Random rand;
        public Form1()
        {
            InitializeComponent();
            timer1.Start();
            p = new PacMan(Color.Yellow, 50, 0, 0, 5, PacMan.Alignment.east);
            g = new Grid(500, 500, gridSize);
            ghost = new Ghost("ghost.png", 0, 0, 5, 50);
            rand = new Random();
        }

        

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
            //PaintPacman(e);
           
            
        }

        private void PaintPacman(PaintEventArgs e)
        {
           //openPacMan - Status 3 
            e.Graphics.FillPie(new SolidBrush(Color.Black), new Rectangle(10, 10, 50, 50), 30, 300);
            //middlePacMan - Status 2
            e.Graphics.FillPie(new SolidBrush(Color.Black), new Rectangle(10, 10, 50, 50), 20, 320);
                //closedPacMan - Status 1
            e.Graphics.FillPie(new SolidBrush(Color.Black), new Rectangle(10, 10, 50, 50), 1, 359);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Weiter Programmieren
            p.OpenClose();
            
           score += p.Move(g, timer1);
           //ghost.Move(rand.Next(0, 100), gridSize, pictureBox1.Height, pictureBox1.Width);
            pictureBox1.Invalidate();
            label2.Text = score.ToString();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
           // p.oldA = p.a;
            switch (e.KeyCode)
            {
                case Keys.Up:
                    {
                        
                        p.newA = PacMan.Alignment.north;
                       // p.turnNorth(p.oldA);
                        break;
                    }
                case Keys.Right:
                    {
                        p.newA = PacMan.Alignment.east;
                     //  p.turnEast(p.oldA);
                        break;
                    }
                case Keys.Down:
                    {
                        p.newA = PacMan.Alignment.south;
                      // p.turnSouth(p.oldA);
                        break;
                    }
                case Keys.Left:
                    {
                        p.newA = PacMan.Alignment.west;
                      // p.turnWest(p.oldA);
                        break;
                    }
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //DrawHorizontal(e);
            //DrawVertical(e);
            //PaintLines(e);
            g.DrawRect(e, pictureBox1);
            g.DrawPills(e, pictureBox1);
            p.drawMe(e);
            //ghost.DrawMe(pictureBox1);
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
