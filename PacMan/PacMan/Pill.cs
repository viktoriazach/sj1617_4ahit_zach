﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace PacMan
{
    class Pill
    {
        public int value;
        public Color c;
        public Point pos;
        public Pill(int v, Color c, int x, int y)
        {
            value = v;
            this.c = c;
            pos = new Point(x, y);
        }
        public void DrawMe(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(c), new Rectangle(pos.X, pos.Y, value, value));
        }
    }
}
