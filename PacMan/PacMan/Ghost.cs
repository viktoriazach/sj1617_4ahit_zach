﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace PacMan
{
    class Ghost
    {
        public int x;
        public int y;
        string filename;
        int step;
        int width;
        PictureBox p = new PictureBox();
        PacMan.Alignment a;

        public Ghost(string f, int sx, int sy, int stepwidth, int w)
        {
            x = sx;
            y = sy;
            p.Image = Image.FromFile(f);
            filename = f;
            step = stepwidth;
            p.Top = x;
            p.Height = y;
            width = w;
            p.Height = w;
            p.Width = w;
            p.SizeMode = PictureBoxSizeMode.Zoom;
            a = PacMan.Alignment.east;
        }

        public Point DrawMe()
        {
            return new Point(x, y);
        }

        public void Move(int rand, int gridSize, int maxheight, int maxwidth)
        {
            if (a == PacMan.Alignment.south)
                y += step;
            if (a == PacMan.Alignment.east)
                x += step;
            if (a == PacMan.Alignment.north)
                y += step;
            if (a == PacMan.Alignment.west)
                x -= step;
            if ((x % (gridSize * 2) == 0) && (y % (gridSize * 2) == 0))
            {
                if (rand <= 25)
                    a = PacMan.Alignment.east;
                else if (rand <= 50)
                    a = PacMan.Alignment.north;
                else if (rand <= 75)
                    a = PacMan.Alignment.south;
                else
                    a = PacMan.Alignment.west;
            }

            if(x < 0)
                x = maxwidth - 10;
            if (x >= maxwidth)
                x = -10;
            if (y < 0)
                y = maxheight - 10;
            if (y >= maxheight)
                y = -10;
            
            
        }

    }
}
