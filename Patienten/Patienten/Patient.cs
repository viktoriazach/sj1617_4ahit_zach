﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patienten
{
    class Patient
    {
        public int id;
        public string vname;
        public string nname;
        public string svnr;

        public Patient(int i, string v, string n, string nr)
        {
            id = i;
            vname = v;
            nname = n;
            svnr = nr;
        }
    }
}
