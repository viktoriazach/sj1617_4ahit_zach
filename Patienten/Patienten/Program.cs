﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Xml;

namespace Patienten
{
    class Program
    {
        
        static void Main(string[] args)
        {
            List<Patient> patients = new List<Patient>();
            List<Treatment> treatments = new List<Treatment>();
            List<Pat_Treat> pts = new List<Pat_Treat>();
            string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Transfer.accdb";

            OleDbConnection con = new OleDbConnection(conStr);
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            patients = Read_Patients(con);
            treatments = Read_Treatments(con);
            pts = Read_Pts(con);

            XML_Pat_Treat(patients, treatments, pts);

            PlausibilityCheck(con);

            try
            {
                con.Close();
            }
            catch (Exception ez)
            {

                Console.WriteLine(ez.Message);
            }



        }

        public static void XML_Pat_Treat(List<Patient> pat, List<Treatment> treat, List<Pat_Treat> pts)
        {
            string s = "<?xml version=\"1.0\" encoding=\"utf-8\"?><root></root>";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(s);

            foreach (Patient p in pat)
            {
                XmlNode xn = doc.CreateElement("Patient");
                xn.InnerText = "";

                XmlNode root = doc.SelectSingleNode("root");
                root.AppendChild(xn);

                XmlNode id = doc.CreateElement("ID");
                id.InnerText = p.id.ToString();
                xn.AppendChild(id);
                XmlNode Nname = doc.CreateElement("Nname");
                Nname.InnerText = p.nname.ToString();
                xn.AppendChild(Nname);
                XmlNode Vname = doc.CreateElement("Vname");
                Vname.InnerText = p.vname.ToString();
                xn.AppendChild(Vname);
                XmlNode SVNR = doc.CreateElement("SVNR");
                SVNR.InnerText = p.svnr.ToString();
                xn.AppendChild(SVNR);
                foreach (Pat_Treat pt in pts)
                {
                    if(pt.id == p.id)
                    {
                        XmlNode beh = doc.CreateElement("Treatment");
                        beh.InnerText = "";
                        xn.AppendChild(beh);
                        XmlNode dauer = doc.CreateElement("Duration");
                        dauer.InnerText = pt.dauer.ToString();
                        beh.AppendChild(dauer);
                        XmlNode date = doc.CreateElement("Date");
                        date.InnerText = pt.date;
                        beh.AppendChild(date);
                        XmlNode behid = doc.CreateElement("TreatmentID");
                        behid.InnerText = pt.beh.ToString();
                        beh.AppendChild(behid);
                        XmlNode behnam = doc.CreateElement("TreatmentName");
                        foreach (Treatment t in treat)
	{
        if (t.id == pt.beh)
        {
            behnam.InnerText = t.bez;
            beh.AppendChild(behnam);
        }
	}
                        
                        



                    }
                }
            }


            doc.Save("Pat_Treat.xml");
        }

        public static List<Patient> Read_Patients(OleDbConnection con)
        {
            List<Patient> patients = new List<Patient>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT ID, Nachname, Vorname, SVNR FROM PATIENT";
            OleDbDataReader reader = cmd.ExecuteReader();
            Patient p;

            while(reader.Read())
            {
                p = new Patient(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3));
                patients.Add(p);
                //Console.WriteLine(reader.GetInt32(0) + reader.GetString(1) + reader.GetString(2) + reader.GetString(3) + "");
            }
            reader.Close();
            return patients;
        }

        public static List<Treatment> Read_Treatments(OleDbConnection con)
        {
            List<Treatment> treatments = new List<Treatment>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT ID, Bezeichnung FROM BEHANDLUNG";
            OleDbDataReader reader = cmd.ExecuteReader();
            Treatment t;
            while (reader.Read())
            {
                t = new Treatment(reader.GetInt32(0), reader.GetString(1));
                treatments.Add(t);
            }
            reader.Close();
            return treatments;
        }

        public static List<Pat_Treat> Read_Pts(OleDbConnection con)
        {
            List<Pat_Treat> pts = new List<Pat_Treat>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
             cmd.CommandText = "SELECT PatientID, BehandlungsID, Dauer, Datum FROM PatientBehandlungen";
            OleDbDataReader reader = cmd.ExecuteReader();
            Pat_Treat pt;
            while (reader.Read())
            {
                pt = new Pat_Treat(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetDateTime(3).ToShortDateString());
                pts.Add(pt);
            }
            return pts;
        }

        public static void PlausibilityCheck(OleDbConnection con)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Pat_Treat.xml");
            XmlNodeList patnode = doc.SelectNodes("root/Patient");
            XmlNodeList treatnode = doc.SelectNodes("root/Patient/Treatment");
            Console.WriteLine("Anzahl Patienten XML: "+patnode.Count);
            Console.WriteLine("Anzahl Behandlungen XML: " + treatnode.Count);

            List<Patient> pat = Read_Patients(con);
            List<Pat_Treat> pts = Read_Pts(con);

            Console.WriteLine("Anzahl Patienten DB: " + pat.Count);
            Console.WriteLine("Anzahl Behandlungen DB: " + pts.Count);

        }



    }
}
